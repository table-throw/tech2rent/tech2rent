define deploy_prod
        docker cp tech2rent:/var/www/html/api/public/media/. /home/webadmin/shared/api/public/media/.
        rm -rf /home/webadmin/tech2rent/
        git clone git@gitlab.com:table-throw/tech2rent/tech2rent.git
endef

define copy_shared
        cp /home/webadmin/shared/.env.local /home/webadmin/tech2rent/api/.env.local
        cp /home/webadmin/shared/client/.env /home/webadmin/tech2rent/client/.env
        cp -Rf /home/webadmin/shared/api/public/media /home/webadmin/tech2rent/api/public/
        mkdir -p /home/webadmin/tech2rent/api/var/cache/prod/
endef

define docker
        docker build --tag tech2rent:1.0 /home/webadmin/tech2rent/.
        docker container rm -f tech2rent
        docker run -tid -p 8080:80 --name tech2rent tech2rent:1.0
endef

define setup
        docker exec -t tech2rent php api/bin/console d:s:u --force
        docker exec -t tech2rent chmod -R 777 api/var/
endef

deploy:
        $(call deploy_prod)
        $(call copy_shared)
        $(call docker)
        $(call setup)
