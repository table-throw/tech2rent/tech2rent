export default {
  path: '/admin',
  name: 'admin',
  component: () => import('../views/admin/AdminPanel'),
  meta: {
    requiresLoggedIn: true,
    requiresCompanyRole: true
  },
  redirect: {
    name: 'adminHome'
  },
  children: [
    {
      name: 'adminHome',
      path: '/',
      component: () => import('../components/admin/AdminHome')
    },
    {
      name: 'adminCompany',
      path: 'company',
      component: () => import('../components/admin/Company')
    },
    {
      name: 'adminStores',
      path: 'stores',
      component: () => import('../components/admin/store/List')
    },
    {
      name: 'adminStoresForm',
      path: 'stores/edit/:id?',
      component: () => import('../components/admin/store/Form'),
      props: true,
    },
    {
      name: 'adminCollaboratorsList',
      path: 'collaborators',
      component: () => import('../components/admin/collaborators/List')
    },
    {
      name: 'adminCollaboratorsForm',
      path: 'collaborators/new',
      component: () => import('../components/admin/collaborators/Form')
    },
  ]
};
