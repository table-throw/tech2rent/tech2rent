export default {
  path: '/stocks',
  name: 'stocks',
  component: () => import('../components/stock/Layout'),
  redirect: { name: 'StockList' },
  children: [
    {
      name: 'StockList',
      path: '',
      component: () => import('../views/stock/List')
    },
    {
      name: 'StockCreate',
      path: 'new',
      component: () => import('../views/stock/Create')
    },
    {
      name: 'StockUpdate',
      path: ':id/edit',
      component: () => import('../views/stock/Update')
    },
    {
      name: 'StockShow',
      path: ':id',
      component: () => import('../views/stock/Show')
    }
  ]
};
