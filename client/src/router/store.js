export default {
  path: '/stores',
  name: 'stores',
  component: () => import('../components/store/Layout'),
  redirect: { name: 'StoreList' },
  children: [
    {
      name: 'StoreList',
      path: '',
      component: () => import('../views/store/List')
    },
    {
      name: 'StoreCreate',
      path: 'new',
      component: () => import('../views/store/Create')
    },
    {
      name: 'StoreUpdate',
      path: ':id/edit',
      component: () => import('../views/store/Update')
    },
    {
      name: 'StoreShow',
      path: ':id',
      component: () => import('../views/store/Show')
    }
  ]
};
