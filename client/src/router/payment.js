export default {
  path: '/payment',
  name: 'payment',
  component: () => import('../components/payment/Layout'),
  redirect: { name: 'PaymentList' },
  children: [
    {
      name: 'Payment',
      path: '',
      component: () => import('../views/payment/Show')
    },
  ]
};
