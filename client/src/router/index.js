import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/index';

//Routes
import categoryRoutes from './category';
import commentRoutes from './comment';
import companyRoutes from './company';
import composeRoutes from './compose';
import orderRoutes from './order';
import productRoutes from './product';
import adminProductRoutes from './admin/product';
import promoCodeRoutes from './admin/promo-code';
import adminCartCompanyRoutes from './admin/cart-company';
import paymentRoutes from './payment';
import cartRoutes from './cart';
import stockRoutes from './stock';
import storeRoutes from './store';
import statRoutes from './admin/stat';

import {
  profileRoutes,
  verifyEmailRoutes,
  forgetPassRoute,
} from './user';

import {
  loginRoute,
  registerRoute
} from './login';

import adminRoutes from './admin';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    name: 'home',
    path: '/',
    component: () => import('../views/home/Homepage'),
  },
    categoryRoutes, commentRoutes, companyRoutes, orderRoutes, composeRoutes, productRoutes, stockRoutes, storeRoutes, profileRoutes, adminCartCompanyRoutes,
    verifyEmailRoutes, loginRoute, forgetPassRoute, registerRoute, adminRoutes, cartRoutes, paymentRoutes, adminProductRoutes, promoCodeRoutes, statRoutes,
  {
    path: '/*',
    redirect: '/'
  },
  ]
});

router.beforeEach((to, from, next) => {
  let loggedIn = store.getters['user/getLoggedIn'];
  if (to.matched.some(record => record.meta.requiresCompanyRole)) {
    let userRoles = store.getters['user/getRoles'];
    if (loggedIn && (userRoles.includes('ROLE_ADMIN') || userRoles.includes('ROLE_COMPANY'))) {
      next();
      return;
    } else {
      next("/")
      return;
    }
  }
  if (to.matched.some(record => record.meta.requiresLoggedOut)) {
    if (loggedIn) {
      next("/")
      return;
    } else {
      next();
      return;
    }
  } else if (to.matched.some(record => record.meta.requiresLoggedIn)) {
    if (loggedIn) {
      next();
      return;
    } else {
      next("/")
      return;
    }
  } else {
    next();
  }
});

export default router;
