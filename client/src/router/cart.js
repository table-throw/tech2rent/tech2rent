export default {
  path: '/carts',
  name: 'carts',
  component: () => import('../components/cart/Layout'),
  redirect: { name: 'CartList' },
  meta: {
    requiresLoggedIn: true
  },
  children: [
    {
      name: 'CartList',
      path: '',
      component: () => import('../views/cart/List')
    },
  ]
};
