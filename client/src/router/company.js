export default {
  path: '/companies',
  name: 'companies',
  component: () => import('../components/company/Layout'),
  redirect: { name: 'CompanyList' },
  children: [
    {
      name: 'CompanyList',
      path: '',
      component: () => import('../views/company/List')
    },
    {
      name: 'CompanyCreate',
      path: 'new',
      component: () => import('../views/company/Create')
    },
    {
      name: 'CompanyUpdate',
      path: ':id/edit',
      component: () => import('../views/company/Update')
    },
    {
      name: 'CompanyShow',
      path: ':id',
      component: () => import('../views/company/Show')
    }
  ]
};
