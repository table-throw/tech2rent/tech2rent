export const profileRoutes = {
  path: '/profile',
  name: 'profile',
  component: () => import('../views/user/Profile'),
  redirect: {
    name: 'profileUser'
  },
  meta: {
    requiresLoggedIn: true
  },
  children: [
    {
      name: 'profileUser',
      path: 'user',
      component: () => import('../components/user/Infos')
    },
    {
      name: 'profileAdress',
      path: 'adress',
      component: () => import('../components/user/Adress')
    },
    {
      name: 'profileOrders',
      path: 'orders/:id?',
      component: () => import('../components/user/Orders')
    }
  ]
};

export const verifyEmailRoutes = {
    name: 'profileVerifyEmail',
    path: '/verifyEmail/:token',
    component: () => import('../views/user/VerifyEmail')
}

export const forgetPassRoute = {
  name: 'resetPassword',
  path: '/resetPassword',
  component: () => import('../views/user/ForgetPassword'),
  meta: {
    requiresLoggedOut: true
  },
  redirect: {
    name: 'resetPasswordEmail'
  },
  children: [{
      name: 'resetPasswordEmail',
      path: 'email',
      component: () => import('../components/user/EmailInputPassword')
    },
    {
      name: 'resetPasswordNew',
      path: 'new/:token?',
      component: () => import('../components/user/newPassword')
    }
  ]
}