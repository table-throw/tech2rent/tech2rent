export default {
  path: '/products',
  name: 'products',
  component: () => import('../components/product/Layout'),
  redirect: { name: 'ProductList' },
  children: [
    {
      name: 'ProductList',
      path: '',
      component: () => import('../views/product/List')
    },
    {
      name: 'ProductShow',
      path: ':id',
      component: () => import('../views/product/Show')
    }
  ]
};
