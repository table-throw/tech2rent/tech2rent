// export default {
//   path: '/orders',
//   name: 'orders',
//   component: () => import('../components/order/Layout'),
//   redirect: {
//     name: 'OrderList'
//   }
// };
export const loginRoute =  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/Login'),
    meta: {
      requiresLoggedOut: true
    }
  }

export const registerRoute =  {
    path: '/register',
    name: 'register',
    component: () => import('../views/login/Register'),
    meta: {
      requiresLoggedOut: true
    }
  }



