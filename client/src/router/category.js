export default {
  path: '/categories',
  name: 'categories',
  component: () => import('../components/category/Layout'),
  redirect: { name: 'CategoryList' },
  children: [
    {
      name: 'CategoryList',
      path: '',
      component: () => import('../views/category/List')
    },
    {
      name: 'CategoryCreate',
      path: 'new',
      component: () => import('../views/category/Create')
    },
    {
      name: 'CategoryUpdate',
      path: ':id/edit',
      component: () => import('../views/category/Update')
    },
    {
      name: 'CategoryShow',
      path: ':id',
      component: () => import('../views/category/Show')
    }
  ]
};
