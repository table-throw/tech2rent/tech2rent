export default {
  path: '/composes',
  name: 'composes',
  component: () => import('../components/compose/Layout'),
  redirect: { name: 'ComposeList' },
  children: [
    {
      name: 'ComposeList',
      path: '',
      component: () => import('../views/compose/List')
    },
    {
      name: 'ComposeCreate',
      path: 'new',
      component: () => import('../views/compose/Create')
    },
    {
      name: 'ComposeUpdate',
      path: ':id/edit',
      component: () => import('../views/compose/Update')
    },
    {
      name: 'ComposeShow',
      path: ':id',
      component: () => import('../views/compose/Show')
    }
  ]
};
