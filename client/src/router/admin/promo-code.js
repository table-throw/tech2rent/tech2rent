export default {
    path: '/admin/promo-codes',
    name: 'admin-promo-codes',
    component: () => import('../../views/admin/AdminPanel'),
    redirect: { name: 'PromoCodeList' },
    children: [
        {
            name: 'PromoCodeList',
            path: '',
            component: () => import('../../views/admin/promo-code/List')
        },
        {
            name: 'PromoCodeCreate',
            path: 'new',
            component: () => import('../../views/admin/promo-code/Create')
        },
        {
            name: 'PromoCodeUpdate',
            path: ':id/edit',
            component: () => import('../../views/admin/promo-code/Update')
        },
    ]
};
