export default {
    path: '/admin/products',
    name: 'admin-products',
    component: () => import('../../views/admin/AdminPanel'),
    redirect: { name: 'AdminProductList' },
    children: [
        {
            name: 'AdminProductList',
            path: '',
            component: () => import('../../views/admin/product/List')
        },
        {
            name: 'AdminProductCreate',
            path: 'new',
            component: () => import('../../views/admin/product/Create')
        },
        {
            name: 'AdminProductUpdate',
            path: ':id/edit',
            component: () => import('../../views/admin/product/Update')
        },
        {
            name: 'AdminProductShow',
            path: ':id',
            component: () => import('../../views/admin/product/Show')
        }
    ]
};
