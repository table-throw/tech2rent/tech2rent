export default {
  path: '/admin/cart-company',
  name: 'admin-cart-company',
  component: () => import('../../components/admin/cart-company/Layout'),
  redirect: { name: 'AdminCartCompanyList' },
  children: [
    {
      name: 'AdminCartCompanyList',
      path: '',
      component: () => import('../../views/admin/cart-company/List')
    },
  ]
};
