export default {
    path: '/admin/stats',
    name: 'admin-stats',
    component: () => import('../../views/admin/AdminPanel'),
    redirect: { name: 'AdminStatList' },
    children: [
        {
            name: 'AdminStatList',
            path: '',
            component: () => import('../../views/admin/stat/List')
        },
    ]
};
