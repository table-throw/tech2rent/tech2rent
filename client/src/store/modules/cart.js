import { getField, updateField } from 'vuex-map-fields';
import SubmissionError from '../../error/SubmissionError';

const handleError = (commit, e) => {
  commit(ACTIONS.TOGGLE_LOADING);

  if (e instanceof SubmissionError) {
    // eslint-disable-next-line
    commit(ACTIONS.SET_ERROR, e.errors._error);

    return;
  }

  // eslint-disable-next-line
  commit(ACTIONS.SET_ERROR, e.message);
};

const ACTIONS = {
  SET_ERROR: 'SET_ERROR',
  TOGGLE_LOADING: 'TOGGLE_LOADING',
  SET_CART: 'SET_CART',
  SET_EXCLUDED_DATES: 'SET_EXCLUDED_DATES',
  SET_CART_LOADED_TRUE: 'SET_CART_LOADED_TRUE',
  SET_CART_LOADED_FALSE: 'SET_CART_LOADED_FALSE',
};

export default function makeCrudModule({
  // normalizeRelations = x => x,
  // resolveRelations = x => x,
  service
} = {}) {
  return {
    actions: {
      getCartProduct: ({
        commit
      }) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .getCartActive()
          .then(data => {
            commit(ACTIONS.SET_CART, data);
          })
          .catch(e => handleError(commit, e));
      },
      getCart: ({
        commit
      }) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .getCartActive()
          .then(data => {
            commit(ACTIONS.SET_CART, data);
            commit(ACTIONS.SET_CART_LOADED_TRUE);
          })
          .catch(e => handleError(commit, e));
      },
      addProductCart: ({
        commit
      }, values) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .addProductCart(values)
          .then(data => {
            service
              .getCartActive()
              .then(data => {
                commit(ACTIONS.SET_CART, data);
              })
              .catch(e => handleError(commit, e));
            return data;
          })
          .catch(e => handleError(commit, e));
      },
      removeProductCart: ({
        commit
      }, values) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .removeProductCart(values)
          .then(data => {
            service
              .getCartActive()
              .then(data => {
                commit(ACTIONS.SET_CART, data);
              })
              .catch(e => handleError(commit, e));
            return data;
          })
          .catch(e => handleError(commit, e));
      },
      getExcludedDates: ({
        commit
      }, values) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .getExcludedDates(values)
          .then(data => {
            commit(ACTIONS.SET_EXCLUDED_DATES, data);
          })
          .catch(e => handleError(commit, e));
      },
      toggleCartLoaded: ({
        commit
      }, value) => {
        if (value == true) {
          commit(ACTIONS.SET_CART_LOADED_TRUE)
        } else {
          commit(ACTIONS.SET_CART_LOADED_FALSE)
        }
      }
    },
    getters: {
      getField,
    },
    mutations: {
      updateField,
      [ACTIONS.SET_EXCLUDED_DATES]: (state, data) => {
        Object.assign(state, {
          excludedDates: data,
        });
      },
      [ACTIONS.SET_CART_LOADED_FALSE]: (state) => {
        Object.assign(state, {
          cartLoaded: false
        });
      },
      [ACTIONS.SET_CART_LOADED_TRUE]: (state) => {
        Object.assign(state, {
          cartLoaded: true
        });
      },
      [ACTIONS.SET_CART]: (state, data) => {
        Object.assign(state, {
          cart: data,
        });
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false
        });
      },
      [ACTIONS.TOGGLE_LOADING]: state => {
        Object.assign(state, {
          error: '',
          isLoading: !state.isLoading
        });
      }
    },
    namespaced: true,
    state: {
      cart: [],
      error: '',
      isLoading: false,
      cartLoaded: false,
      excludedDates: [],
    }
  };
}