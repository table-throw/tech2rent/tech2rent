import { getField, updateField } from "vuex-map-fields";
import SubmissionError from "../../error/SubmissionError";

const handleError = (commit, e) => {
  commit(ACTIONS.TOGGLE_LOADING);

  if (e instanceof SubmissionError) {
    // eslint-disable-next-line
    commit(ACTIONS.SET_ERROR, e.errors._error);

    return;
  }

  // eslint-disable-next-line
  commit(ACTIONS.SET_ERROR, e.message);
};

const ACTIONS = {
  SET_ERROR: "SET_ERROR",
  TOGGLE_LOADING: "TOGGLE_LOADING",
  SET_PROMO_CODE: "SET_PROMO_CODE",
  TOGGLE_ACTIVATION: "TOGGLE_ACTIVATION",
  SET_LIST_PROMO_CODE: "SET_LIST_PROMO_CODE",
  ADD_CODE_PROMO: "ADD_CODE_PROMO",
  SET_PROMO_CODE_LOADED: "SET_PROMO_CODE_LOADED",
};

export default function makeCrudModule({
  // normalizeRelations = x => x,
  // resolveRelations = x => x,
  service,
} = {}) {
  return {
    actions: {
      addPromoCode: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .addPromoCode(values)
          .then((data) => {
            commit(ACTIONS.SET_PROMO_CODE, data);
            commit(ACTIONS.ADD_CODE_PROMO, data);
          })
          .catch((e) => handleError(commit, e));
      },
      updatePromoCode: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .updatePromoCode(values)
          .then((data) => {
            commit(ACTIONS.SET_PROMO_CODE, data);
            commit(ACTIONS.ADD_CODE_PROMO, data);
          })
          .catch((e) => handleError(commit, e));
      },
      getPromoCode: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .findPromoCode(values)
          .then((data) => {
            commit(ACTIONS.SET_PROMO_CODE, data);
            commit(ACTIONS.SET_PROMO_CODE_LOADED, true);
          })
          .catch((e) => handleError(commit, e));
      },
      setIsActive: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        commit(ACTIONS.TOGGLE_ACTIVATION, values);
        service
          .setIsActive(values)
          .then((data) => {
            commit(ACTIONS.SET_PROMO_CODE, data);
          })
          .catch((e) => handleError(commit, e));
      },
      checkValidityPromoCode: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .checkPromoCode(values)
          .then((data) => {
            commit(ACTIONS.TOGGLE_LOADING);
            commit(ACTIONS.SET_PROMO_CODE, data);
          })
          .catch((e) => handleError(commit, e));
      },
      getListCodePromo: ({ commit }, values) => {
        commit(ACTIONS.SET_ERROR, "");
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .getPromoCodes(values)
          .then((data) => {
            commit(ACTIONS.TOGGLE_LOADING);
            commit(ACTIONS.SET_LIST_PROMO_CODE, data);
            commit(ACTIONS.SET_PROMO_CODE_LOADED, false);
          })
          .catch((e) => handleError(commit, e));
      },
      setDefaultCodePromo: ({
        commit
      }) => {
        commit(ACTIONS.SET_PROMO_CODE, {});
      }
    },
    getters: {
      getField,
    },
    mutations: {
      updateField,
      [ACTIONS.ADD_CODE_PROMO]: (state, data) => {
        Object.assign(state, {
          listPromoCode: [...state.listPromoCode, data],
        });
      },
      [ACTIONS.SET_PROMO_CODE]: (state, data) => {
        Object.assign(state, {
          promoCode: data,
        });
      },
      [ACTIONS.SET_LIST_PROMO_CODE]: (state, data) => {
        Object.assign(state, {
          listPromoCode: data["hydra:member"],
        });
      },
      [ACTIONS.TOGGLE_ACTIVATION]: (state, data) => {
        data.isActive = !data.isActive;
        Object.assign(state, {
          promoCode: data,
        });
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false,
        });
      },
      [ACTIONS.TOGGLE_LOADING]: (state) => {
        Object.assign(state, {
          error: "",
          isLoading: !state.isLoading,
        });
      },
      [ACTIONS.SET_PROMO_CODE_LOADED]: (state, data) => {
        Object.assign(state, {
          promoCodeLoaded: data,
        });
      },
    },
    namespaced: true,
    state: {
      promoCode: {},
      error: "",
      isLoading: false,
      listPromoCode: [],
      promoCodeLoaded: false,
    },
  };
}
