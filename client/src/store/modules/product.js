import { getField, updateField } from 'vuex-map-fields';
import SubmissionError from '../../error/SubmissionError';

const handleError = (commit, e) => {
  commit(ACTIONS.TOGGLE_LOADING);

  if (e instanceof SubmissionError) {
    // eslint-disable-next-line
    commit(ACTIONS.SET_ERROR, e.errors._error);

    return;
  }

  // eslint-disable-next-line
  commit(ACTIONS.SET_ERROR, e.message);
};

const ACTIONS = {
  CREATE: 'CREATE',
  SET_ERROR: 'SET_ERROR',
  TOGGLE_LOADING: 'TOGGLE_LOADING',
  SET_PRODUCT: 'SET_PRODUCT',
  SET_PRODUCT_LOADED: "SET_PRODUCT_LOADED",
  SET_ALL_PRODUCTS: 'SET_ALL_PRODUCTS',
  SET_ALL_CATEGORIES: 'SET_ALL_CATEGORIES',
  SET_AUTOCOMPLETE: 'SET_AUTOCOMPLETE',
  SET_CREATED: 'SET_CREATED',
  SET_UPDATED: 'SET_UPDATED',
  ADD_TO_UPLOADED_IMAGES: 'ADD_TO_UPLOADED_IMAGES',
  CLEAR_UPLOADED_IMAGES: 'CLEAR_UPLOADED_IMAGES'
};

export default function makeCrudModule({
  // normalizeRelations = x => x,
  // resolveRelations = x => x,
  service
} = {}) {
  return {
    actions: {
      setProductLoadedState: ({
        commit
      }, value) => {
        commit(ACTIONS.SET_PRODUCT_LOADED, value)
      },
      toggleLoading: ({
        commit
      }) => {
        commit(ACTIONS.TOGGLE_LOADING)
      },
      create: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service.addProduct(values)
          .then(data => {
            commit(ACTIONS.SET_ALL_PRODUCTS, data);
            commit(ACTIONS.TOGGLE_LOADING);
            commit(ACTIONS.SET_CREATED, data);
            commit(ACTIONS.CLEAR_UPLOADED_IMAGES)
          })
          .catch(e => handleError(commit, e));
      },
      update: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service.updateProduct(values)
          .then(data => {
            commit(ACTIONS.SET_ALL_PRODUCTS, data);
            commit(ACTIONS.TOGGLE_LOADING);
            commit(ACTIONS.SET_UPDATED, data);
            commit(ACTIONS.CLEAR_UPLOADED_IMAGES)
          })
          .catch(e => handleError(commit, e));
      },
      listProducts: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .getProducts(values)
          .then(data => {
            commit(ACTIONS.SET_ALL_PRODUCTS, data);
            commit(ACTIONS.SET_PRODUCT_LOADED, false)
          })
          .catch(e => handleError(commit, e));
      },
      listCategories: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .getCategories(values)
          .then(data => {
            commit(ACTIONS.SET_ALL_CATEGORIES, data);
          })
          .catch(e => handleError(commit, e));
      },
      autocompleteProduct: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .getAutocomplete(values)
          .then(data => {
            let newData;
            newData = data["hydra:member"].map((titleProduct) => {
              return { name: titleProduct };
            });
            commit(ACTIONS.SET_AUTOCOMPLETE, newData);
          })
          .catch(e => handleError(commit, e));
      },
      uploadProductImages: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        service
          .uploadImages(values)
          .then((data) => {
            commit(ACTIONS.ADD_TO_UPLOADED_IMAGES, data["@id"])
          })
          .catch(e => handleError(commit, e));
      },
      sendSearchProduct: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .searchProductElk(values)
          .then(data => {
            commit(ACTIONS.SET_ALL_PRODUCTS, data);
          })
          .catch(e => handleError(commit, e));
      },
      getProduct: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .findProduct(values)
          .then(data => {
            commit(ACTIONS.SET_PRODUCT, data);
            commit(ACTIONS.SET_PRODUCT_LOADED, true)
          })
          .catch(e => handleError(commit, e));
      },
      addComment: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        service
          .addComment(values.comment)
          .then(() => {
            service
              .findProduct(values.productId)
              .then(data => {
                commit(ACTIONS.SET_PRODUCT, data);
              })
              .catch(e => handleError(commit, e));
          })
          .catch(e => handleError(commit, e));
      },
    },
    getters: {
      getField,
    },
    mutations: {
      updateField,
      [ACTIONS.SET_PRODUCT]: (state, data) => {
        Object.assign(state, {
          product: data,
        });
      },
      [ACTIONS.ADD_TO_UPLOADED_IMAGES]: (state, data) => {
        Object.assign(state, {
          uploadedImages: [...state.uploadedImages, data],
        });
      },
      [ACTIONS.CLEAR_UPLOADED_IMAGES]: (state) => {
        Object.assign(state, {
          uploadedImages: [],
        });
      },
      [ACTIONS.SET_ALL_PRODUCTS]: (state, data) => {
        Object.assign(state, {
          allProducts: data['hydra:member'],
        });
      },
      [ACTIONS.SET_ALL_CATEGORIES]: (state, data) => {
        Object.assign(state, {
          categories: data['hydra:member'],
        });
      },
      [ACTIONS.SET_CREATED]: (state, created) => {
        Object.assign(state, { created });
      },
      [ACTIONS.SET_UPDATED]: (state, updated) => {
        Object.assign(state, { updated });
      },
      [ACTIONS.SET_AUTOCOMPLETE]: (state, data) => {
        Object.assign(state, {
          autocompleteWord: data,
        });
      },
      [ACTIONS.SET_PRODUCT_LOADED]: (state, data) => {
        Object.assign(state, {
          productLoaded: data
        })
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false
        });
      },
      [ACTIONS.TOGGLE_LOADING]: state => {
        Object.assign(state, {
          error: '',
          isLoading: !state.isLoading
        });
      }
    },
    namespaced: true,
    state: {
      allProducts: [],
      product: {},
      productLoaded: false,
      categories: [],
      autocompleteWord: [],
      uploadedImages: [],
      error: '',
      created: null,
      updated: null,
      isLoading: false,
    }
  };
}