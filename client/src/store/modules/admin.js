// import Vue from 'vue';
import {
  getField,
  updateField
} from 'vuex-map-fields';

const initialState = {
  company:{},
  stores:[],
  companyFound: false,
  storesFound: false,
  storesChanged: null,
  error: '',
  isLoading: false
}

const handleError = (commit, e) => {
  commit(ACTIONS.SET_ERROR, e);
};

const ACTIONS = {
  //company
  SET_COMPANY_FOUND: 'SET_COMPANY_FOUND',
  SET_COMPANY_NOT_FOUND: 'SET_COMPANY_NOT_FOUND',
  //stores
  SET_STORES_FOUND: 'SET_STORES_FOUND',
  SET_STORES_NOT_FOUND: 'SET_STORES_NOT_FOUND',
  SET_STORES_CHANGED: 'SET_STORES_CHANGED',
  SET_ERROR: 'SET_ERROR',
  TOGGLE_LOADING: 'TOGGLE_LOADING'
};

export default function makeUserModule({
  // normalizeRelations = x => x,
  resolveRelations = x => x,
  service
} = {}) {
  return {
    actions: {
      // Company
      getCompany: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .findCompany(values)
          .then((data) => {
            if (data['@id'] && data['@id'].length > 0) {
              commit(ACTIONS.SET_COMPANY_FOUND, data);
            } else {
              commit(ACTIONS.SET_COMPANY_NOT_FOUND);
            }
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      createCompany: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .createCompany(values)
          .then((data) => {
            commit(ACTIONS.SET_COMPANY_FOUND, data);
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      updateCompany: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .updateCompany(values, values.id)
          .then((data) => {
            commit(ACTIONS.SET_COMPANY_FOUND, data);
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      // Stores 
      getStoresByCompany: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .findByCompany(values)
          .then((data) => {            
            if (data['hydra:totalItems'] > 0)  {
              commit(ACTIONS.SET_STORES_FOUND, data['hydra:member']);
            } else {
              commit(ACTIONS.SET_STORES_NOT_FOUND);
            }
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      createStore: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .createStore(values)
          .then(() => {
            commit(ACTIONS.SET_STORES_CHANGED, true);
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      updateStore: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .updateStore(values, values.id)
          .then(() => {
            commit(ACTIONS.SET_STORES_CHANGED, true);
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      deleteStore: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .deleteStore(values)
          .then(() => {
            commit(ACTIONS.SET_STORES_CHANGED, true);
           commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      resetStoresChanged: ({
        commit
      }) => {
        commit(ACTIONS.SET_STORES_CHANGED, null);
      },
    },
    getters: {
      getField,
      getStoreById: state => id => {
        for (const store of state.stores) {
          if (store.id === id) {
            return resolveRelations(store);
          }
        }
        return null;
      }
    },
    mutations: {
      updateField,
      [ACTIONS.SET_COMPANY_FOUND]: (state, data) => {
        Object.assign(state, {
          company: data,
          companyFound: true,
          error: null
        });
      },
      [ACTIONS.SET_COMPANY_NOT_FOUND]: (state) => {
        Object.assign(state, {
          companyFound: false,
          error: null
        });
      },
      [ACTIONS.SET_STORES_FOUND]: (state, data) => {
        Object.assign(state, {
          stores: data,
          storesFound: true,
          error: null
        });
      },
      [ACTIONS.SET_STORES_NOT_FOUND]: (state) => {
        Object.assign(state, {
          stores: [],
          storesFound: false,
          error: null
        });
      },
      [ACTIONS.SET_STORES_CHANGED]: (state, data) => {
        Object.assign(state, {
          storesChanged: data
        });
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false
        });
      },
      [ACTIONS.TOGGLE_LOADING]: (state,data) => {
        Object.assign(state, {
          error: '',
          isLoading: data
        });
      }
    },
    namespaced: true,
    state: initialState
  };
}