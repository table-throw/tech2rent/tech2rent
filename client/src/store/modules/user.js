// import Vue from 'vue';
import {
  getField,
  updateField
} from 'vuex-map-fields';
import moment from 'moment';
// import remove from 'lodash/remove';

const user = JSON.parse(localStorage.getItem('user'));

const userInfos = user ? {
  loggedIn: true,
  userInfos: user,

} : {
  loggedIn: false,
  userInfos: null,
};

const initialState = {
  ...userInfos,
  orders:null,
  tempProfilePicture: null,
  verified: false,
  error: '',
  isLoading: false,
  isRegistered: false,
  loadProfile: false,
  passwordChanged: null,
  emailSent: null,
}

const handleApiUserData = (data) => {
  let userInfos = {
    ...data
  };
  userInfos.birthday =  data.birthday ? moment(data.birthday).format('DD/MM/YYYY') : '';
  return userInfos;
}

const handleError = (commit, e) => {
  commit(ACTIONS.SET_ERROR, e);
};

const ACTIONS = {
  SET_LOGIN: 'SET_LOGIN',
  SET_REGISTERED: 'SET_REGISTERED',
  SET_PASSWORD_CHANGED: 'SET_PASSWORD_CHANGED',
  SET_EMAIL_SENT: 'SET_EMAIL_SENT',
  SET_PROFILE_PICTURE: 'SET_PROFILE_PICTURE',
  SET_LOGOUT: 'SET_LOGOUT',
  SET_USERINFOS: 'SET_USERINFOS',
  SET_USER_ORDERS: 'SET_USER_ORDERS',
  SET_VERIFIED: 'SET_VERIFIED',
  SET_ERROR: 'SET_ERROR',
  TOGGLE_LOADING: 'TOGGLE_LOADING',
  SET_TRUE_LOAD_PROFILE: 'SET_TRUE_LOAD_PROFILE',
  SET_FALSE_LOAD_PROFILE: 'SET_FALSE_LOAD_PROFILE'
};

export default function makeUserModule({
  // normalizeRelations = x => x,
  // resolveRelations = x => x,
  service
} = {}) {
  return {
    actions: {
      register: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .register(values)
          .then(() => {
            commit(ACTIONS.SET_REGISTERED, true);
            commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      login: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .login(values)
          .then(data => {
            localStorage.setItem('user', JSON.stringify(data.data));
            localStorage.setItem('token', JSON.stringify({
              jwt: data.token,
              refreshToken: data.refresh_token
            }));
            commit(ACTIONS.SET_LOGIN, data);
            commit(ACTIONS.TOGGLE_LOADING, false);

          })
          .catch(e => handleError(commit, e));
      },
      logout: ({
        commit
      }) => {
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        commit(ACTIONS.SET_LOGOUT);
      },
      getProfile: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .find(values)
          .then(data => {
            let newUserInfos = handleApiUserData(data);
            commit(ACTIONS.SET_USERINFOS, newUserInfos);
            commit(ACTIONS.SET_TRUE_LOAD_PROFILE);
            commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      getOrders: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .getOrders(values)
          .then(data => {
            if (data && data['hydra:totalItems'] > 0) {
              commit(ACTIONS.SET_USER_ORDERS, data['hydra:member']);
            } else {
              commit(ACTIONS.SET_USER_ORDERS, []);
            }
            commit(ACTIONS.TOGGLE_LOADING, false);
          })
          .catch(e => handleError(commit, e));
      },
      setLoadProfileFalse: ({
        commit
      }) => {
        commit(ACTIONS.SET_FALSE_LOAD_PROFILE);
      },
      updateProfile: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .update(values.user, values.userId)
          .then(data => {
            let newUserInfos = handleApiUserData(data);
            commit(ACTIONS.SET_USERINFOS, newUserInfos);
            commit(ACTIONS.TOGGLE_LOADING, false);

          })
          .catch(e => handleError(commit, e));
      },
      updatePassword: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .updatePassword(values.token, values.newPass)
          .then(data => {
            if (data && data.success && data.success === 'password changed') {
              commit(ACTIONS.SET_PASSWORD_CHANGED, true);
            } else {
              commit(ACTIONS.SET_PASSWORD_CHANGED, false);
            }
            commit(ACTIONS.TOGGLE_LOADING, false);

          })
          .catch(e => handleError(commit, e));
      },
      verifyEmail: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .verifyEmail(values)
          .then((data) => {
            if (data) {
              let newUserInfos = handleApiUserData(data)
              commit(ACTIONS.SET_USERINFOS, newUserInfos);
              commit(ACTIONS.SET_VERIFIED);
              commit(ACTIONS.TOGGLE_LOADING, false);
            } else {
              commit(ACTIONS.SET_ERROR, 'Already verified');
            }
          })
          .catch(e => handleError(commit, e));
      },
      sendEmailForgetPassword: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .sendEmail(values)
          .then((data) => {
            if (data && data.success && data.success === 'email send') {
              commit(ACTIONS.SET_EMAIL_SENT, true);
              commit(ACTIONS.TOGGLE_LOADING, false);
            } else {
              commit(ACTIONS.SET_ERROR, 'not found');
            }
          })
          .catch(e => handleError(commit, e));
      },
      resetSentEmail: ({
        commit
      }) => {
        commit(ACTIONS.SET_EMAIL_SENT, null);
      },
      resetPasswordChanged: ({
        commit
      }) => {
        commit(ACTIONS.SET_PASSWORD_CHANGED, null);
      },
      resetIsRegistered: ({
        commit
      }) => {
        commit(ACTIONS.SET_REGISTERED, null);
      },
      uploadProfilePicture: ({
        commit
      }, values) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING, true);
        service
          .uploadImage(values)
          .then((data) => {
            commit(ACTIONS.SET_PROFILE_PICTURE, {
              imageId: data['@id'],
              imageUrl: data.contentUrl
            });
            commit(ACTIONS.TOGGLE_LOADING, false);

          })
          .catch(e => handleError(commit, e));
      }
    },
    getters: {
      getField,
      getLoggedIn(state) {
        return state.loggedIn;
      },
      getRoles(state) {
        if (state.userInfos && state.userInfos.roles) {
          return state.userInfos.roles;
        }
        return [];
      },
    },
    mutations: {
      updateField,
      [ACTIONS.SET_LOGIN]: (state, data) => {
        Object.assign(state, {
          userInfos: data.data,
          loggedIn: true
        });
      },
      [ACTIONS.SET_REGISTERED]: (state, data) => {
        Object.assign(state, {
          isRegistered: data
        });
      },
      [ACTIONS.SET_PASSWORD_CHANGED]: (state, data) => {
        Object.assign(state, {
          passwordChanged: data
        });
      },
      [ACTIONS.SET_EMAIL_SENT]: (state, data) => {
        Object.assign(state, {
          emailSent: data
        });
      },
      [ACTIONS.SET_PROFILE_PICTURE]: (state, data) => {
        Object.assign(state, {
          tempProfilePicture: data
        });
      },
      [ACTIONS.SET_LOGOUT]: (state) => {
        Object.assign(state, {
          userInfos: null,
          loggedIn: false
        });
      },
      [ACTIONS.SET_USERINFOS]: (state, data) => {
        Object.assign(state, {
          userInfos: data,
          error: ''
        });
      },
      [ACTIONS.SET_USER_ORDERS]: (state, data) => {
        Object.assign(state, {
          orders: data,
          error: '',

        });
      },
      [ACTIONS.SET_VERIFIED]: (state) => {
        Object.assign(state, {
          verified: true,
          error: ''
        });
      },
      [ACTIONS.SET_TRUE_LOAD_PROFILE]: state => {
        Object.assign(state, {
          loadProfile: true
        });
      },
      [ACTIONS.SET_FALSE_LOAD_PROFILE]: (state) => {
        Object.assign(state, {
          loadProfile: false
        });
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false
        });
      },
      [ACTIONS.TOGGLE_LOADING]: (state,data) => {
        Object.assign(state, {
          isLoading: data
        });
      }
    },
    namespaced: true,
    state: initialState,
  };
}
