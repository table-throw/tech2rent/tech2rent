import { getField, updateField } from 'vuex-map-fields';
import SubmissionError from '../../error/SubmissionError';

const handleError = (commit, e) => {
  commit(ACTIONS.TOGGLE_LOADING);

  if (e instanceof SubmissionError) {
    // eslint-disable-next-line
    commit(ACTIONS.SET_ERROR, e.errors._error);

    return;
  }

  // eslint-disable-next-line
  commit(ACTIONS.SET_ERROR, e.message);
};

const ACTIONS = {
  SET_ERROR: 'SET_ERROR',
  TOGGLE_LOADING: 'TOGGLE_LOADING',
  SET_CART: 'SET_CART',
  SET_PAYMENT_VALIDATED_TRUE: 'SET_PAYMENT_VALIDATED_TRUE',
  SET_PAYMENT_VALIDATED_FALSE: 'SET_PAYMENT_VALIDATED_FALSE',
};

export default function makeCrudModule({
  // normalizeRelations = x => x,
  // resolveRelations = x => x,
  service
} = {}) {
  return {
    actions: {
      sendCharge: ({
        commit
      }, values) => {
          commit(ACTIONS.SET_ERROR, '');
          commit(ACTIONS.TOGGLE_LOADING);
        service
          .sendCharge(values)
          .then(() => {
            commit(ACTIONS.SET_PAYMENT_VALIDATED_TRUE);
          })
          .catch(e => handleError(commit, e));
      },
      passPaymentFalse: ({
        commit
      }) => {
        commit(ACTIONS.SET_ERROR, '');
        commit(ACTIONS.TOGGLE_LOADING);
        commit(ACTIONS.SET_PAYMENT_VALIDATED_FALSE);
      },
    },
    getters: {
      getField,
    },
    mutations: {
      updateField,
      [ACTIONS.SET_PAYMENT_VALIDATED_TRUE]: state => {
        Object.assign(state, {
          paymentValidated: true
        });
      },
      [ACTIONS.SET_PAYMENT_VALIDATED_FALSE]: state => {
        Object.assign(state, {
          paymentValidated: false
        });
      },
      [ACTIONS.SET_ERROR]: (state, error) => {
        Object.assign(state, {
          error,
          isLoading: false
        });
      },
      [ACTIONS.TOGGLE_LOADING]: state => {
        Object.assign(state, {
          error: '',
          isLoading: !state.isLoading
        });
      }
    },
    namespaced: true,
    state: {
      error: '',
      isLoading: false,
      paymentValidated: false,
    }
  };
}