import Vue from 'vue';
import Vuex from 'vuex';
import makeCrudModule from './modules/crud';
import makeUserModule from './modules/user';
import makeProductModule from './modules/product';
import makeCartModule from './modules/cart';
import makePaymentModule from './modules/payment';
import makeAdminModule from './modules/admin';
import notifications from './modules/notifications';
import makePromoCodeModule from './modules/promoCode';

import paymentService from '../services/payment';
import categoryService from '../services/category';
import commentService from '../services/comment';
import companyService from '../services/company';
import composeService from '../services/compose';
import orderService from '../services/order';
import productService from '../services/product';
import cartService from '../services/cart';
import stockService from '../services/stock';
import storeService from '../services/store';
import userService from '../services/user';
import adminService from '../services/admin';
import adminProductService from '../services/adminProduct';
import adminPromoCodeService from '../services/adminPromoCode';
import promoCodeService from '../services/promoCode';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    notifications,
    category: makeCrudModule({
      service: categoryService
    }),
    comment: makeCrudModule({
      service: commentService
    }),
    company: makeCrudModule({
      service: companyService
    }),
    compose: makeCrudModule({
      service: composeService
    }),
    adminProduct: makeCrudModule({
      service: adminProductService
    }),
    adminPromoCode: makePromoCodeModule({
      service: adminPromoCodeService
    }),
    order: makeCrudModule({
      service: orderService
    }),
    product: makeProductModule({
      service: productService
    }),
    promoCode: makeCrudModule({
      service: promoCodeService
    }),
    cart: makeCartModule({
      service: cartService
    }),
    stock: makeCrudModule({
      service: stockService
    }),
    store: makeCrudModule({
      service: storeService
    }),
    user: makeUserModule({
      service: userService
    }),
    payment: makePaymentModule({
      service: paymentService
    }),
    admin: makeAdminModule({
      service: adminService
    })
  },
  strict: process.env.NODE_ENV !== 'production'
});

export default store;