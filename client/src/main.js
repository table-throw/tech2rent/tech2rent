import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueMask from 'v-mask';
import App from './App.vue';
import vuetify from './plugins/vuetify';

import store from './store';
import router from './router';
import i18n from './i18n';

import {
  startRefreshToken
} from './utils/utilsMethods';

const initVueApp = () => {
  Vue.config.productionTip = false;

  Vue.use(Vuelidate);
  Vue.use(VueMask);

  new Vue({
    i18n,
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app');
}


let token = JSON.parse(localStorage.getItem('token'));
if (token && token.refreshToken) {
  startRefreshToken()
    .then(() => {
      initVueApp();
    })
    .catch(() => {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      initVueApp();
    });
} else {
  initVueApp();
}

