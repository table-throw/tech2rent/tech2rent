import fetch from '../utils/fetch';

const endpointCompany = 'companies';
const endpointStore = 'stores';

export default {
  // Company endpoints
  createCompany(payload) {
    return fetch(endpointCompany, {
        method: 'POST',
        body: JSON.stringify(payload)
      })
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  findCompany(id) {
    return fetch(`${endpointCompany + '/' +id}`)
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  updateCompany(payload, id) {
    return fetch(`${endpointCompany + '/' + id}`, {
        method: 'PATCH',
        body: JSON.stringify(payload)
      })
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  // Store endpoints
  createStore(payload) {
    return fetch(endpointStore, {
        method: 'POST',
        body: JSON.stringify(payload)
      })
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  findByCompany(id) {
    return fetch(`${endpointStore + '?company=/companies/' +id}`)
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  updateStore(payload, id) {
    return fetch(`${endpointStore + '/' + id}`, {
        method: 'PATCH',
        body: JSON.stringify(payload)
      })
      .then(response => {
        return response.json();
      })
      .catch(e => {
        throw e;
      });
  },
  deleteStore(id) {
    return fetch(`${endpointStore + '/' + id}`, {
        method: 'DELETE'
      })
      .then(response => {
        return response;
      })
      .catch(e => {
        throw e;
      });
  }
}
