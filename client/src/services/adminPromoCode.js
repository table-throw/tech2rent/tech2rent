import fetch from "../utils/fetch";

const promocode = "promotional_codes";
export default {
  addPromoCode(datas) {
    return fetch(promocode, {
      method: "POST",
      body: JSON.stringify(datas),
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  updatePromoCode(datas) {
    return fetch(promocode + "/" + datas.id, {
      method: "PATCH",
      body: JSON.stringify(datas),
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  getPromoCodes() {
    return fetch(promocode + "/", {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  findPromoCode(id) {
    return fetch(promocode + "/" + id, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  setIsActive(datas) {
    return fetch(promocode + "/" + datas.id, {
      method: "PATCH",
      body: JSON.stringify(datas),
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
  checkPromoCode(code) {
    return fetch(promocode + "/check/" + code, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
};
