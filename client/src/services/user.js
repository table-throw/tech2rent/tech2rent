import fetch from '../utils/fetch';

const endpoint = 'users';
const endpointUpload = 'media_objects';
const endpointOrders = 'carts';
const endpointAuth = 'authentication_token';

export default {
  register(payload) {
    return fetch(endpoint, {
          method: 'POST',
          body: JSON.stringify(payload)
    })
    .then(response => response.json())
    .catch(e => {
      throw e;
    });
  },
  login(payload) {
    return fetch(endpointAuth, {
          method: 'POST',
          body: JSON.stringify(payload)
    })
    .then(response => {
      return response.json();
    })
    .catch(e => {
      throw e;
    });
  },
  find(id) {
    return fetch(`${endpoint + '/' +id}`)
    .then(response => {
      return response.json();
    })
    .catch(e => {
      throw e;
    });
  },
  getOrders() {
    return fetch(endpointOrders)
    .then(response => {
      return response.json();
    })
    .catch(e => {
      throw e;
    });
  },
  update(payload, userId) {
    return fetch(`${endpoint + '/' + userId}`, {
          method: 'PATCH',
          body: JSON.stringify(payload)
    })
    .then(response => {
      return response.json();
    })
    .catch(e => {
      throw e;
    });
  },
  updatePassword(token, newPassword) {
    return fetch(`${endpoint + '/' + token + '/reset/' + newPassword}`)
    .then(response => {
      return response.json();
    })
    .catch(e => {
      throw e;
    });
  },
  sendEmail(email) {
    return fetch(`${endpoint + /forgot-password/ + email}`)
      .then(response => response.json())
      .catch(e => {
        throw e;
      });
  },
  verifyEmail(token) {
    return fetch(`${endpoint + '/validation/' + token}`)
      .then(response => response.json())
      .catch(e => {
        throw e;
      });
  },
  uploadImage(formData) {
    return fetch(endpointUpload, {
       method: 'POST',
       body: formData
    })
    .then(response => response.json())
    .catch(e => {
      throw e;
    });
  }
  // findAll(params) {
  //   return fetch(endpoint, params);
  // },
  // create(payload) {
  //   return fetch(endpoint, {
  //     method: 'POST',
  //     body: JSON.stringify(payload)
  //   });
  // },
  // del(item) {
  //   return fetch(item['@id'], {
  //     method: 'DELETE'
  //   });
  // },
  // update(payload) {
  //   return fetch(payload['@id'], {
  //     method: 'PUT',
  //     body: JSON.stringify(payload)
  //   });
  // }
}



