import fetch from '../utils/fetch';

const payment = 'carts/payment/';

export default {
  sendCharge(tab) {
    // var myHeaders = new Headers({
    //   "Authorization": 'Bearer ' + JSON.parse(localStorage.getItem("user")).token
    // });
    var products = tab.cart.products;
    if(tab.cart.promoCode !== undefined){
      this.patchPromoCodeCart(tab.cart);
    }
    return Promise.all(products.cartProducts.map((value, index) => {
      return fetch("cart_products/" + value.id, {
        method: 'PATCH',
        // headers: myHeaders,
        body: JSON.stringify({
          dateStart: tab.cart.startDates[index],
          dateEnd: tab.cart.endDates[index],
        })
      });
    })).then(
      () => {
        return fetch(payment+tab.token, {
          method: 'GET',
          // headers: myHeaders
        })
        .then(response => response.json())
        .catch(e => {
          throw e;
        })
      }
    );
  },
  patchPromoCodeCart(item){
    return fetch("/promotional_codes/" + item.promoCode.id + "/add-cart/" + item.products.id, {
      method: 'PATCH'
      // headers: myHeaders
    })
    .then(response => { 
      return response.json();
      })
    .catch(e => {
      throw e;
    });
  }
}
