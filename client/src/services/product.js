import fetch from "../utils/fetch";

const products = "products";
const categories = "categories";
const comments = "comments";
const endpointUpload = "media_objects";

export default {
  uploadImages(formData) {
    return fetch(endpointUpload, {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
  findProduct(id) {
    return fetch(products + "/" + id, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  getProducts() {
    return fetch(products + "/search", {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  getCategories() {
    return fetch(categories, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  getAutocomplete(word) {
    return fetch(products + "/autocomplete/" + word, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  searchProductElk(url) {
    return fetch(products + "/search?" + url, {
      method: "GET",
    })
      .then((response) => response.json())
      .catch((e) => e);
  },
  addComment(payload) {
    return fetch(comments, {
      method: "POST",
      body: JSON.stringify(payload),
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
  addProduct(payload) {
    if (payload.product.isNew === undefined) {
      payload.product.isNew = false;
    }
    return fetch(products, {
      method: "POST",
      body: JSON.stringify({
        name: payload.product.name,
        price: payload.product.price,
        caution: payload.product.caution,
        description: payload.product.description,
        isNew: payload.product.isNew,
        store: "/stores/" + payload.product.store.id,
        category: "/categories/" + payload.product.category.id,
        images: payload.images,
      }),
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
  updateProduct(payload) {
    return fetch(products + "/" + payload.product.id, {
      method: "PATCH",
      body: JSON.stringify({
        name: payload.product.name,
        price: payload.product.price,
        caution: payload.product.caution,
        description: payload.product.description,
        isNew: payload.product.isNew,
        store: payload.product.store["@id"],
        category: payload.product.category["@id"],
        images: payload.images,
      }),
    })
      .then((response) => response.json())
      .catch((e) => {
        throw e;
      });
  },
};
