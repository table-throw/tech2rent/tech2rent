import fetch from '../utils/fetch';

const carts = 'carts';

export default {
  getCartActive() {
    return fetch(carts + "/owned", {
      method: 'GET',
    })
    .then(response => response.json())
    .catch(e => e);
  },
  addProductCart(ids) {
    return fetch(carts + "/" + ids.cartId + "/add-product/" + ids.productId, {
      method: 'GET',
    })
    .then(response => response.json())
    .catch(e => e);
  },
  removeProductCart(ids) {
    return fetch(carts + "/" + ids.cartId + "/remove-product/" + ids.productId, {
      method: 'GET',
    })
    .then(response => response.json())
    .catch(e => e);
  },
  getExcludedDates(id) {
    return fetch(carts + "/dispo-product/" + id, {
      method: 'GET',
    })
    .then(response => response.json())
    .catch(e => e);
  },
}
