import fetch from './fetch';

const endpointRefreshToken = 'api/token/refresh';

const postRefreshToken = (payload) => {
      return fetch(endpointRefreshToken, {
          method: 'POST',
          body: JSON.stringify(payload)
        })
        .then(response => {
          return response.json()
            .then(data => {
               localStorage.setItem('token', JSON.stringify({
                 jwt: data.token,
                 refreshToken: data.refresh_token
               }));
               return true;
            });
        })  
        .catch(e => {
          throw e;
        });
}

export const startRefreshToken = () => {
   let token = JSON.parse(localStorage.getItem('token'));
  //  setInterval(function () {
  //     postRefreshToken({
  //       refresh_token: token.refreshToken
  //     });
  //   }, 115 * 1000);
   return postRefreshToken({
     refresh_token: token.refreshToken
   });
}