# PHP BUILD
FROM php:7.4-alpine as build_api

RUN apk update && apk upgrade
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

WORKDIR /app
COPY ./api ./

# build dev for tests
#RUN echo "APP_ENV=dev" >> .env.local && composer install \
#	&& php vendor/bin/behat \
#	&& rm -r vendor/


RUN composer install
#RUN composer install --no-dev --optimize-autoloader

## NODE BUILD
FROM node:12.18-alpine as build_client

RUN apk update && apk upgrade

WORKDIR /app
COPY ./client ./
RUN yarn install \
	&& yarn build

# DEPLOYMENT
FROM php:7.4-apache as deployment

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y libicu-dev libzip-dev zip

# Install Postgre PDO
RUN docker-php-ext-install pdo pdo_mysql

COPY --from=build_api /app ./api
COPY --from=build_client /app ./client

#RUN openssl req -new -newkey rsa:4096 -days 3650 -sha256 -nodes -x509 -subj \
#        "/C=FR/ST=tech2rent/L=tech2rent/O=tech2rent/CN=actu.dev" \
#        -keyout /etc/ssl/private.key -out /etc/ssl/cert.crt

RUN rm /etc/apache2/sites-available/000-default.conf

COPY /production.docker/apache.conf/docker/tech2rent.conf /etc/apache2/sites-available/tech2rent.conf
COPY /production.docker/apache.conf/docker/api.tech2rent.conf /etc/apache2/sites-available/api.tech2rent.conf


RUN a2enmod ssl && a2enmod rewrite && a2ensite tech2rent.conf && a2ensite api.tech2rent.conf

CMD ["apachectl", "-D", "FOREGROUND"]

EXPOSE 80


