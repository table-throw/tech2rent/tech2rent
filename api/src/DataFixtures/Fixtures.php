<?php

namespace App\DataFixtures;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Company;
use App\Entity\Compose;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Stock;
use App\Entity\Store;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Fixtures extends Fixture
{
    private $encoder;
    private $categories;
    private $nameProduct;

    private $maxCategory;
    private $maxStore;
    private $maxProduct;
    private $maxStock;
    private $maxComment;
    private $maxCompany;
    private $maxUser;
    private $maxOrder;
    private $maxCompose;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->categories = ['Perceuses', 'Smartphone', 'Casque', 'Smartphone Android', 'Smartphone Apple', 'Macbook', 'PC'];
        $this->nameProduct = ['Iphone X', 'Iphone 10', 'Iphone 11', 'Iphone 11 Pro', 'P30 Pro', 'P30', 'P30 lite', 'P20', 'Samsung Galaxy S10', 'Samsung Galaxy S20'];

        $this->maxCategory = count($this->categories);
        $this->maxStore = 200;
        $this->maxProduct = 300;
        $this->maxStock = 200;
        $this->maxComment = 300;
        $this->maxCompany = 13;
        $this->maxUser = 300;
        $this->maxOrder = 300;
        $this->maxCompose = 300;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        // company fixtures
        for ($i = 0; $i < $this->maxCompany; $i++) {
            $company = (new Company())
                ->setName($faker->company)
                ->setStreet($faker->streetAddress)
                ->setCity($faker->city)
                ->setCountry($faker->country)
                ->setZc($faker->countryCode)
                ->setSiret($faker->uuid)
            ;
            $manager->persist($company);
        }
        $manager->flush();

        // category fixtures
        for ($i = 0; $i < $this->maxCategory; $i++) {
            $category = (new Category())
                ->setTitle(($this->categories)[$i])
                ->setDescription($faker->text('200'))
            ;
            $manager->persist($category);
        }
        $manager->flush();

        // company fixtures
        for ($i = 0; $i < $this->maxCompany; $i++) {
            $company = (new Company())
                ->setName($faker->company)
                ->setStreet($faker->streetAddress)
                ->setCity($faker->city)
                ->setCountry($faker->country)
                ->setZc($faker->countryCode)
                ->setSiret($faker->uuid)
            ;
            $manager->persist($company);
        }
        $manager->flush();
        
        // store fixtures
        for ($i = 0; $i < $this->maxStore; $i++) {
            $store = (new Store())
                ->setStreet($faker->streetAddress)
                ->setCity($faker->city)
                ->setCountry($faker->country)
                ->setZc($faker->countryCode)
                ->setCompany(($manager->getRepository(Company::class)->findAll())[rand(0, ($this->maxCompany)-1)])
            ;
            $manager->persist($store);
        }
        $manager->flush();

        // stock fixtures
        // for ($i = 0; $i < $this->maxStock; $i++) {
        //     $stock = (new Stock())
        //         ->setQuantity($faker->numberBetween(0, 5))
        //         ->setStore(($manager->getRepository(Store::class)->findAll())[rand(0, ($this->maxStore)-1)])
        //     ;
        //     $manager->persist($stock);
        // }
        // $manager->flush();

        // product fixtures
        for ($i = 0; $i < $this->maxProduct; $i++) {
            $product = (new Product())
                ->setName(($this->nameProduct)[rand(0, count($this->nameProduct)-1)])
                ->setPrice($faker->randomFloat(2, 500, 1000))
                ->setDescription($faker->text('200'))
                ->setNotation(0)
                ->setIsNew($faker->randomElement([0, 1]))
                ->setCategory(($manager->getRepository(Category::class)->findAll())[rand(0, ($this->maxCategory)-1)])
                ->setStore(($manager->getRepository(Store::class)->findAll())[rand(0, ($this->maxStore)-1)])
            ;
            $manager->persist($product);
        }
        $manager->flush();
        
        // CartProduct fixtures
        for ($i = 0; $i < $this->maxProduct; $i++) {
            $CartProduct = (new CartProduct())
                ->setProduct(($manager->getRepository(Product::class)->findAll())[rand(0, ($this->maxProduct)-1)])
                ->setDateEnd($faker->dateTime)
                ->setDateStart($faker->dateTime)
            ;
            $manager->persist($CartProduct);

           
        }
        $manager->flush();

        // user fixtures
        for ($i = 0; $i < $this->maxUser; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            if ($gender == 'male'): $firstName = $faker->firstNameMale; elseif ($gender == 'female'): $firstName = $faker->firstNameFemale;endif;

            $user = new User();
            $user->setEmail($faker->email)
                ->setRoles(['ROLE_USER'])
                ->setFirstName($firstName)
                ->setLastName($faker->lastName)
                ->setPhone($faker->phoneNumber)
                ->setBirthday($faker->dateTime)
                ->setAlias($faker->lastName.rand(1000, 2000))
                ->setIsActive(true)
                ->setGender($gender)
                ->setPassword($this->encoder->encodePassword($user, $faker->password(12, 12)))
                ->setCompany($manager->getRepository(Company::class)->findAll()[rand(0, ($this->maxCompany)-1)])
            ;
            $manager->persist($user);
        }

        // user fixtures
        for ($i = 0; $i < $this->maxUser; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            if ($gender == 'male'): $firstName = $faker->firstNameMale; elseif ($gender == 'female'): $firstName = $faker->firstNameFemale;endif;

            $user = new User();
            $user->setEmail($faker->email)
                ->setRoles(['ROLE_USER'])
                ->setFirstName($firstName)
                ->setLastName($faker->lastName)
                ->setPhone($faker->phoneNumber)
                ->setBirthday($faker->dateTime)
                ->setAlias($faker->lastName.rand(1000, 2000))
                ->setIsActive(true)
                ->setGender($gender)
                ->setPassword($this->encoder->encodePassword($user, $faker->password(12, 12)))
                // ->setCompany($manager->getRepository(Company::class)->findAll()[rand(0, ($this->maxCompany)-1)])
            ;
            $manager->persist($user);
        }

        $user = (new User())
            ->setEmail('admin@admin.fr')
            ->setRoles(['ROLE_ADMIN'])
            ->setFirstName('admin_firstname')
            ->setLastName('admin_lastname')
            ->setPhone('+33 6 71 27 09 46')
            ->setBirthday($faker->dateTime)
            ->setAlias('froger')
            ->setIsActive(true)
            ->setGender('male')
            ->setCompany($manager->getRepository(Company::class)->findAll()[rand(0, ($this->maxCompany)-1)])
            ->setPassword($this->encoder->encodePassword($user, 'admin123'))
        ;
        $manager->persist($user);
        $manager->flush();
        
        //user company
        $user = (new User())
            ->setEmail('campany@campany.fr')
            ->setRoles(['ROLE_COMPANY'])
            ->setFirstName('company_firstname')
            ->setLastName('company_lastname')
            ->setPhone('+33 6 71 27 09 46')
            ->setBirthday($faker->dateTime)
            ->setAlias('frogercommedab')
            ->setIsActive(true)
            ->setGender('male')
            ->setCompany($manager->getRepository(Company::class)->findAll()[rand(0, ($this->maxCompany)-1)])
            ->setPassword($this->encoder->encodePassword($user, 'company123'))
        ;
        $manager->persist($user);
        $manager->flush();

        // cart fixtures
        for ($i = 0; $i < $this->maxProduct; $i++) {
            $cart = (new Cart())
                ->addCartProduct($manager->getRepository(CartProduct::class)->findAll()[rand(0, ($this->maxProduct)-1)])
                ->setCartReference('t'.$i)
                ->setRenter($manager->getRepository(User::class)->findAll()[rand(0, ($this->maxUser)-1)])
            ;
            $manager->persist($cart);

        }
        $manager->flush();

        // comment fixtures
        for ($i = 0; $i < $this->maxComment; $i++) {
            $comment = (new Comment())
                ->setContent($faker->text('120'))
                ->setTitle($faker->text('120'))
                ->setNotation($faker->randomFloat(1, 0, 5))
                ->setProduct(($manager->getRepository(Product::class)->findAll())[rand(0, ($this->maxProduct)-1)])
                ->setCommentator(($manager->getRepository(User::class)->findAll())[rand(0, ($this->maxUser)-1)])
            ;
            $manager->persist($comment);
        }
        $manager->flush();

        // order fixtures
        for ($i = 0; $i < $this->maxOrder; $i++) {
            $order = (new Order())
                ->setTotalPrice($faker->randomFloat(2, 1000, 2000))
                ->setRenter($manager->getRepository(User::class)->findAll()[rand(0, ($this->maxUser)-1)])
            ;
            $manager->persist($order);
        }
        $manager->flush();

        // compose fixtures
        for ($i = 0; $i < $this->maxCompose; $i++) {
            $compose = (new Compose())
                ->setQuantity($faker->numberBetween(1, 5))
                ->setOrder($manager->getRepository(Order::class)->findAll()[rand(0, ($this->maxOrder)-1)])
                ->setProduct($manager->getRepository(Product::class)->findAll()[rand(0, ($this->maxProduct)-1)])
            ;
            $manager->persist($compose);
        }
        $manager->flush();
    }
}
