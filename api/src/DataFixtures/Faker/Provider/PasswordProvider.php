<?php
namespace App\DataFixtures\Faker\Provider;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordProvider
{

    private static $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        self::$encoder = $encoder;
    }

    public static function encoder(String $str): String
    {
        $user = new User();
        return self::$encoder->encodePassword($user, $str);
    }
}
