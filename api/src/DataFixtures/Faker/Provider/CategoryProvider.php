<?php

namespace App\DataFixtures\Faker\Provider;

class CategoryProvider
{
    private static $categoryTitles;

    public function __construct()
    {
        self::$categoryTitles = [
            'Perceuses',
            'Smartphone',
            'Casque',
            'Smartphone Android',
            'Smartphone Apple',
            'Macbook',
            'PC',
            'Tablette',
            'Écran',
            'Cables'
        ];
    }

    public static function getOneCategoryTitle(): String
    {
       if(!empty(self::$categoryTitles ))
       {
            $index = array_rand(self::$categoryTitles);
            //dump($index );
            $value = self::$categoryTitles[$index];

            unset(self::$categoryTitles[$index]);

            return $value;
       }else{
            self::$categoryTitles = [
                'Perceuses',
                'Smartphone',
                'Casque',
                'Smartphone Android',
                'Smartphone Apple',
                'Macbook',
                'PC',
                'Tablette',
                'Écran',
                'Cables'
            ];
            return self::getOneCategoryTitle();
            
       }

    }
}
