<?php

namespace App\DataFixtures\Faker\Provider;

class ProductProvider
{
    public static function getOneProductName(): String
    {
        $nameProduct = [
            'Iphone X',
            'Iphone 10',
            'Iphone 11',
            'Iphone 11 Pro',
            'P30 Pro',
            'P30',
            'P30 lite',
            'P20',
            'Samsung Galaxy S10',
            'Samsung Galaxy S20'
        ];

        return $nameProduct[array_rand($nameProduct)];
    }
}
