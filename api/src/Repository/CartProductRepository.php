<?php

namespace App\Repository;

use App\Entity\CartProduct;
use App\Entity\Store;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CartProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartProduct[]    findAll()
 * @method CartProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartProduct::class);
    }
    
    public function countRentByProduct($companyId, $store)
    {
        $entityManager = $this->getEntityManager();
        
        $whereStore = '';
        if ($store instanceof Store) {
            $whereStore = ' and s.id = :storeId';
        }
        
        $query = $entityManager->createQuery(
             'SELECT count(cp.id) as count, p.id, p.name
        FROM App\Entity\CartProduct cp
        INNER JOIN cp.product p
        INNER JOIN p.store s
        INNER JOIN s.company c
        WHERE c.id = :companyId '.$whereStore.'
        GROUP BY p.id, p.name'
         )->setParameter('companyId', $companyId);

        if ($store instanceof Store) {
            $storeId = $store->getId();
            $query->setParameter('storeId',$storeId);
        }
         
         return $query->getResult(); 
    }    
    
    public function countRentByCategory($companyId, $store)
    {
        $entityManager = $this->getEntityManager();
        
        $whereStore = '';
        if ($store instanceof Store) {
            $whereStore = ' and s.id = :storeId';
        }
        
        $query = $entityManager->createQuery(
             'SELECT count(cp.id) as count, ca.id, ca.title
        FROM App\Entity\CartProduct cp
        INNER JOIN cp.product p
        INNER JOIN p.category ca
        INNER JOIN p.store s
        INNER JOIN s.company c
        WHERE c.id = :companyId '.$whereStore.'
        GROUP BY ca.id, ca.title'
         )->setParameter('companyId', $companyId);
        
        if ($store instanceof Store) {
            $storeId = $store->getId();
            $query->setParameter('storeId',$storeId);
        }
         return $query->getResult(); 
    }  

    public function findForArrayDate($dateStart, $dateEnd, string $uuid_product) {
        $qb = $this->createQueryBuilder('cp')
            ->innerJoin('cp.cart', 'c')
            ->innerJoin('cp.product', 'p')
            ->where('cp.dateStart > :dateStart')
            ->andWhere('cp.dateEnd < :dateEnd')
            ->andWhere('c.isActive = false')
            ->andWhere('p.id = :uuid_product')
            ->setParameters(['dateStart' => $dateStart, 'dateEnd' => $dateEnd, 'uuid_product' => $uuid_product]);

        return $qb->getQuery()->getResult();
    }
    
    public function averageDayByProduct($companyId, $store)
    {
        $entityManager = $this->getEntityManager();

        $whereStore = '';
        if ($store instanceof Store) {
            $whereStore = ' and s.id = :storeId';
        }
        
        $query = $entityManager->createQuery(
             'SELECT count(p.id) as count ,p.id, p.name, avg(cp.dateEnd -  cp.dateStart) as diff
        FROM App\Entity\CartProduct cp
        INNER JOIN cp.product p
        INNER JOIN p.category ca
        INNER JOIN p.store s
        INNER JOIN s.company c
        WHERE c.id = :companyId '.$whereStore.'
        GROUP BY p.id, p.name'
         )->setParameter('companyId', $companyId);
        
        if ($store instanceof Store) {
            $storeId = $store->getId();
            $query->setParameter('storeId',$storeId);
        }
         return $query->getResult(); 
    }

    public function averageDayByCategory($companyId, $store)
    {
        $entityManager = $this->getEntityManager();

        $whereStore = '';
        if ($store instanceof Store) {
            $whereStore = ' and s.id = :storeId';
        }

        $query = $entityManager->createQuery(
            'SELECT count(cp.id) as count, ca.id, ca.title, avg(cp.dateEnd -  cp.dateStart) as diff
        FROM App\Entity\CartProduct cp
        INNER JOIN cp.product p
        INNER JOIN p.category ca
        INNER JOIN p.store s
        INNER JOIN s.company c
        WHERE c.id = :companyId '.$whereStore.'
        GROUP BY ca.id, ca.title'
        )->setParameter('companyId', $companyId);

        if ($store instanceof Store) {
            $storeId = $store->getId();
            $query->setParameter('storeId',$storeId);
        }
        return $query->getResult();
    }



    /**
     * @return CartProduct[] Returns an array of Cart objects
     */

    public function findCartsProductByCompany($idCompany)
    {
        return $this->createQueryBuilder('cp')
            ->innerJoin('cp.product', 'p')
            ->innerJoin('p.store', 's')
            ->innerJoin('s.company', 'co')
            ->andWhere('co.id = :idCompany')
            ->setParameter('idCompany', $idCompany)
            ->getQuery()
            ->getResult()
            ;
    }
}
