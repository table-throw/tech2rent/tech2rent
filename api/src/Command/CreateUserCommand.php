<?php
namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;



class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    private $entityManager;
    private $passwordEncoder;


    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this->setDescription('Creates a new user')
            ->setHelp('This command allows you to create a user')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password',InputArgument::REQUIRED, 'User password')
            ->addArgument('role',InputArgument::REQUIRED, 'Role User')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $io = new SymfonyStyle($input, $output);
        $io->title('Creates a User');

        $email = $input->getArgument('email');
        $passwordToEncode = $input->getArgument('password');
        $role = $input->getArgument('role');

        $user = new User();
        $password = $this->passwordEncoder->encodePassword($user, $passwordToEncode);

        $user
            ->setEmail($email)
            ->setPassword($password)
            ->setFirstName(bin2hex(random_bytes(5)))
            ->setLastName(bin2hex(random_bytes(5)))
            ->setPhone('+33 0 00 00 00 00')
            ->setAlias(bin2hex(random_bytes(5)))
            ->setIsActive(true)
            ->setRoles([$role])
        ;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln('User successfully generated!');
        exit();
    }
}
