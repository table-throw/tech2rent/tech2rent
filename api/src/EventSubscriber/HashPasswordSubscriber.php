<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

final class HashPasswordSubscriber implements EventSubscriberInterface
{
    private $passwordEncoder;
    private $mailer;
    private $requestStack;
    private $token;
    private $security;

    public function __construct(RequestStack $requestStack, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer,Security $security)
    {
        $this->requestStack = $requestStack;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->security = $security;
        $this->token = uniqid();

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['encodePassword', EventPriorities::PRE_WRITE]
        ];
    }

    public function encodePassword(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $roles = $user->getRoles();

        $connectedUser = $this->security->getUser();
        if (in_array('ROLE_ADMIN',$roles) && empty($connectedUser)) {
            if (!in_array('ROLE_ADMIN',$connectedUser->getRoles())) {
                throw new AccessDeniedException('You Can\'t create admin.');
            }
        }

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);
        $user->setToken($this->token);

        $message = (new \Swift_Message('Validation de votre compte utilisateur'))
            ->setFrom('no-reply@tech2rent.fr')
            ->setTo($user->getEmail())
            ->setBody($_SERVER['FRONT_URL'].'/verifyEmail/'.$this->token);
        $this->mailer->send($message);
    }
    
}
