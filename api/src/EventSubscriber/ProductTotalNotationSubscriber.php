<?php


namespace App\EventSubscriber;

use App\Entity\Comment;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class ProductTotalNotationSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }
    

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof Comment) {
            
            $entityManager = $args->getObjectManager();
            $product = $entity->getProduct();

            $productComments = $product->getComments();
            
            $product->addComment($entity);
            
            $sommeNotation = 0;
            $key = 0;
            
            foreach ($productComments as $key => $productComment) {
                $sommeNotation += $productComment->getNotation();
                
            }
            $notation = $sommeNotation / ($key +1);
            
            $product->setNotation($notation);
            $entityManager->persist($product);
            $entityManager->flush();     
        }
    }
}
