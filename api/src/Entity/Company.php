<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"company_item_get"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "patch"={
 *              "normalization_context"={"groups"={"company_item_patch"}},
 *              "security"="is_granted('ROLE_ADMIN') or (object.getId() == user.getCompany().getId())"
 *          }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "normalization_context"={"groups"={"company_item_post"}},
 *          }
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "logo": "exact", "city": "partial", "renters": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"company_item_get", "carts_items_get", "company_item_patch", "company_item_post"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get", "company_item_patch","company_item_post"})
     *
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $zc;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $siret;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Store", mappedBy="company", orphanRemoval=true)
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $stores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     * @Groups({"company_item_get", "company_item_patch","company_item_post"})
     */
    private $renters;
    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @Groups({"get_profile", "users_items_post", "products_items_get","company_item_get", "company_item_patch", "company_item_post"})
     * @ORM\JoinColumn(nullable=true)
     */
    public $logo;

    public function __construct()
    {
        $this->renters = new ArrayCollection();
        $this->stores = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?MediaObject
    {
        return $this->logo;
    }

    public function setLogo(MediaObject $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getZc(): ?string
    {
        return $this->zc;
    }

    public function setZc(string $zc): self
    {
        $this->zc = $zc;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }


    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->name);
    }

    /**
     * @return Collection|store[]
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }

    public function addStore(store $store): self
    {
        if (!$this->stores->contains($store)) {
            $this->stores[] = $store;
            $store->setCompany($this);
        }

        return $this;
    }

    public function removeStore(store $store): self
    {
        if ($this->stores->contains($store)) {
            $this->stores->removeElement($store);
            // set the owning side to null (unless already changed)
            if ($store->getCompany() === $this) {
                $store->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getRenters()
    {
        return $this->renters;
    }

    public function addRenter(User $renter): self
    {
        if (!$this->renters->contains($renter)) {
            $this->renters[] = $renter;
            $renter->setCompany($this);
        }

        return $this;
    }

    public function removeRenter(User $renter): self
    {
        if ($this->renters->contains($renter)) {
            $this->renters->removeElement($renter);
            // set the owning side to null (unless already changed)
            if ($renter->getCompany() === $this) {
                $renter->setCompany(null);
            }
        }

        return $this;
    }
}
