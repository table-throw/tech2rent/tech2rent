<?php
namespace App\Entity\CustomRoutes;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Stats\CountRentByProduct;
use App\Controller\Stats\CountRentByCategory;
use App\Controller\Stats\GetBestCategory;
use App\Controller\Stats\AverageDayByProduct;
use App\Controller\Stats\AverageDayByCategory;

/**
 * @ApiResource(
 *     collectionOperations={},
 *     itemOperations={
 *          "average_day_by_product"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "path"="/stats/average-day-by-product/{uuid_store}",
 *              "controller"=AverageDayByProduct::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Get average day by product",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_store",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="Average day by product",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Store not found",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "average_day_by_category"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "path"="/stats/average-day-by-category/{uuid_store}",
 *              "controller"=AverageDayByCategory::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Get average day by category",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_store",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="Average day by category",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Store not found",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "count_rent_by_product"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "path"="/stats/count-rent-by-product/{uuid_store}",
 *              "controller"=CountRentByProduct::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Get the number of rentals per product",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_store",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="The number of rentals per product",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Store not found",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "count_rent_by_category"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "path"="/stats/count-rent-by-category/{uuid_store}",
 *              "controller"=CountRentByCategory::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Get the number of rentals per product",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_store",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="The number of rentals per product",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Store not found",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "get_best_category"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "path"="/stats/get-best-category",
 *              "controller"=GetBestCategory::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Get the number of rentals per product",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_store",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="The number of rentals per product",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Store not found",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *     }
 * )
 */
class Stats
{
}
