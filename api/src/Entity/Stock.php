<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"products_items_get"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('ROLE_COMPANY')",
 *          }
 *     }
 * )    
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"products_items_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"products_items_get"})
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store")
     * @Groups({"products_items_get","carts_items_get"})
     */
    private $store;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $product;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->id);
    }
}
