<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CartProductRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Cart\GetCartByCompany;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *    itemOperations={
 *          "get"={},
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_USER')"
 *          }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('ROLE_USER')",
 *          },
 *          "get_cart_company"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"cart_product_item_get"}},
 *              "defaults"={"_api_receive"=false},
 *              "path"="/cart_products/companies",
 *              "controller"=GetCartByCompany::class,
 *              "security"="is_granted('ROLE_COMPANY')",
 *          },
 *     }
 * ) * @ORM\Entity(repositoryClass=CartProductRepository::class)
 */
class CartProduct
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"carts_items_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"carts_items_get","cart_product_item_get"})
     */
    private $dateStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"carts_items_get","cart_product_item_get"})
     */
    private $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity=Cart::class, inversedBy="cartProducts")
     * @Groups({"cart_product_item_get"})
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="cartProducts")
     * @Groups({"carts_items_get","cart_product_item_get"})

     */
    private $product;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
