<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"products_items_get"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_COMPANY')"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_COMPANY')"
 *          }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_COMPANY')",
 *          }
 *     }
 * )  
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "company": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\StoreRepository")
 */
class Store
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @Groups({"company_item_get"})
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get","cart_product_item_get"})
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get","cart_product_item_get"})

     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get","cart_product_item_get"})

     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get","cart_product_item_get"})
     */
    private $zc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="stores")
     * @Groups({"carts_items_get"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="store")
     * @Groups({"company_item_get"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getZc(): ?string
    {
        return $this->zc;
    }

    public function setZc(string $zc): self
    {
        $this->zc = $zc;

        return $this;
    }

    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->id);
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setStore($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getStore() === $this) {
                $product->setStore(null);
            }
        }

        return $this;
    }

}
