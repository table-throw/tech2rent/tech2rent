<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\User\UserValidationController;
use App\Controller\User\UserPatch;
use App\Controller\User\UserForgotPassword;
use App\Controller\User\UserPatchPassword;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"= {
 *                  "normalization_context"={"groups"={"get_profile"}},
*                   "security"="is_granted('ROLE_ADMIN')"
 *            },
 *          "post"={
 *                  "normalization_context"={"groups"={"users_items_post"}}
 *         },
*          "check_user"={
*              "method"="GET",
*              "path"="/users/validation/{token}",
*              "controller"=UserValidationController::class,
*              "defaults"={"_api_receive"=false},
*              "openapi_context"= {
*                  "parameters"= {
*                      {
*                          "name": "token",
*                          "in": "path",
*                          "type": "string",
*                          "required": true
*                      }
*                  }
*              }
*          },
 *      },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"get_profile"}},
 *              "security"="is_granted('ROLE_ADMIN') or object.getId() == user.getId()"
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getId() == user.getId()"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getId() == user.getId()"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getId() == user.getId()",
 *              "controller"=UserPatch::class,
 *          },
 *          "changePassword"={
 *              "method"="GET",
 *              "path"="/users/{token}/reset/{password}",
 *              "controller"=UserPatchPassword::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"= {
 *                  "parameters"= {
 *                      {
 *                          "name": "token",
 *                          "in": "path",
 *                          "type": "string",
 *                          "required": true
 *                      },{
 *                          "name": "password",
 *                          "in": "path",
 *                          "type": "string",
 *                          "required": true
 *                      }
 *                  }
 *              }
 *          },
 *          "forgotten_password"={
 *              "method"="GET",
 *              "path"="/users/forgot-password/{email}",
 *              "controller"=UserForgotPassword::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"= {
 *                  "parameters"= {
 *                      {
 *                          "name": "email",
 *                          "in": "path",
 *                          "type": "string",
 *                          "required": true
 *                      }
 *                  }
 *              } 
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "email": "exact", "firstName": "partial"})
 * @ORM\Table(name="`user`")
 */

class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({ "users_items_post", "get_profile"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"users_items_post", "get_profile", "company_item_get", "company_item_patch"})
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json", nullable= true)
     * @Groups("get_profile")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_items_get", "get_profile","users_items_post", "company_item_get","company_item_patch","cart_product_item_get"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get_profile", "users_items_post", "products_items_get", "company_item_get", "company_item_patch","cart_product_item_get"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable= true)
     * @Groups("get_profile")
     */
    private $phone;

    /**
     * @ORM\Column(type="date", nullable= true)
     * @Groups("get_profile")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable= true)
     * @Groups("get_profile")
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable= true)
     * @Groups("get_profile")
     */
    private $gender;

    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @Groups({"get_profile", "users_items_post", "products_items_get"})
     * @ORM\JoinColumn(nullable=true)
     */
    public $avatar;
    
    public $plainPassword;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="renters")
     * @Groups("get_profile")
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Order", mappedBy="renter")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->carts = new ArrayCollection();
    }


    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="renter")
     */
    private $carts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get_profile", "company_item_get", "company_item_patch"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_profile")
     */
    private $billingStreet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_profile")
     */
    private $billingCity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_profile")
     */
    private $billingCountry;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_profile")
     */
    private $billingZc;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAvatar(): ?MediaObject
    {
        return $this->avatar;
    }

    public function setAvatar(MediaObject $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setRenter($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getRenter() === $this) {
                $order->setRenter(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->id);
    }
    
    public function getActiveCart()
    {
        $cart = null;
        foreach ($this->carts as $cart) {
            if ($cart->getIsActive()) break;
        }
        return $cart;
    }
    
    /**
     * @return Collection|Cart[]
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts[] = $cart;
            $cart->setRenter($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->contains($cart)) {
            $this->carts->removeElement($cart);
            // set the owning side to null (unless already changed)
            if ($cart->getRenter() === $this) {
                $cart->setRenter(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getBillingStreet(): ?string
    {
        return $this->billingStreet;
    }

    public function setBillingStreet(?string $billingStreet): self
    {
        $this->billingStreet = $billingStreet;

        return $this;
    }

    public function getBillingCity(): ?string
    {
        return $this->billingCity;
    }

    public function setBillingCity(?string $billingCity): self
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    public function getBillingCountry(): ?string
    {
        return $this->billingCountry;
    }

    public function setBillingCountry(?string $billingCountry): self
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    public function getBillingZc(): ?string
    {
        return $this->billingZc;
    }

    public function setBillingZc(?string $billingZc): self
    {
        $this->billingZc = $billingZc;

        return $this;
    }
    
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }
    
}
