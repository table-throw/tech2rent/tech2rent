<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(itemOperations={
 *          "get"={
 *
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getCommentator().getId() == user.getId()"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getCommentator().getId()  == user.getId()"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_ADMIN') or object.getCommentator().getId()  == user.getId()"
 *          }
 *     }) * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"products_items_get"})
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="comments")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"products_items_get"})
     */
    private $commentator;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"products_items_get"})
     */
    private $notation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"products_items_get"})
     */
    private $title;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->id);
    }

    public function getCommentator(): ?User
    {
        return $this->commentator;
    }

    public function setCommentator(?User $commentator): self
    {
        $this->commentator = $commentator;

        return $this;
    }

    public function getNotation(): ?float
    {
        return $this->notation;
    }

    public function setNotation(float $notation): self
    {
        $this->notation = $notation;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
