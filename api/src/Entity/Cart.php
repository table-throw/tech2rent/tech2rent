<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Cart\DispoProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\Cart\ApplyCode;
use App\Controller\Cart\AddProduct;
use App\Controller\Cart\GetOwnedActiveCart;
use App\Controller\Cart\GetCartByCompany;
use App\Controller\Cart\CreateCharge;
use App\Controller\Cart\Get;
use App\Controller\Cart\RemoveProduct;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "security"="is_granted('ROLE_USER')",
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_USER')"
 *          }
 *     },
 *     collectionOperations={
 *         "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"carts_items_get"}},
 *              "defaults"={"_api_receive"=false},
 *              "path"="/carts",
 *              "controller"=Get::class,
 *              "security"="is_granted('ROLE_USER')",
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_USER')"
 *           },
 *          "get_dispo_products"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "path"="/carts/dispo-product/{uuid_cart}",
 *              "controller"=DispoProduct::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Create charge token",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_cart",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="Dispo of product",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Product or cart not found.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              },
 *          },
 *          "get_my_active_cart"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "path"="/carts/owned",
 *              "controller"=GetOwnedActiveCart::class,
 *              "defaults"={"_api_receive"=false},
 *              "normalization_context"={"groups"={"carts_items_get"}},
 *          },
 *          "post_card_charge"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "path"="/carts/payment/{token}",
 *              "controller"=CreateCharge::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Create charge token",
 *                  "parameters"= {
 *                      {
 *                          "name" : "token",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true
 *                      }
 *                  }
 *              }
 *          },
 *          "add-product"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "input_formats"={"json"={"application/merge-patch+json"}},
 *              "path"="/carts/{uuid_cart}/add-product/{uuid_product}",
 *              "controller"=AddProduct::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Add product into cart.",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_cart",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      },
 *                      {
 *                          "name" : "uuid_product",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="The product has been added to cart.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Product or cart not found.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              },
 *          },
 *          "remove-product"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "path"="/carts/{uuid_cart}/remove-product/{uuid_product}",
 *              "controller"=RemoveProduct::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Remove a product from cart.",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_cart",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      },
 *                      {
 *                          "name" : "uuid_product",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "204"={
 *                          "description"="The product has been removed from cart.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Product or cart not found.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              },
 *          },
 *          "apply-promot-code"={
 *              "method"="GET",
 *              "security"="is_granted('ROLE_USER')",
 *              "path"="/carts/apply-code/{code}/{uuid_cart}",
 *              "controller"=ApplyCode::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Apply a promotional code.",
 *                  "parameters"= {
 *                      {
 *                          "name" : "code",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                      },
 *                      {
 *                          "name" : "uuid_cart",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="A new price with promot code.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Promot code or cart not found.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              },
 *          },
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @Groups({"carts_items_get"})
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"carts_items_get"})
     */
    private $cartReference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="carts")
     * @Groups({"cart_product_item_get"})
     */
    private $renter;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"carts_items_get"})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="cart")
     * @Groups({"carts_items_get"})
     */
    private $cartProducts;

    /**
     * @ORM\ManyToOne(targetEntity=PromotionalCode::class, inversedBy="carts")
     * @Groups({"cart_product_item_get"})
     */
    private $promotionalCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"carts_items_get"})
     */
    private $paymentDate;


    public function __construct()
    {
        $this->cartReference = bin2hex(random_bytes(8));
        $this->isActive = true;
        $this->cartProducts = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCartReference(): ?string
    {
        return $this->cartReference;
    }

    public function setCartReference(string $cartReference): self
    {
        $this->cartReference = $cartReference;

        return $this;
    }

    public function getRenter(): ?User
    {
        return $this->renter;
    }

    public function setRenter(?User $renter): self
    {
        $this->renter = $renter;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setCart($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getCart() === $this) {
                $cartProduct->setCart(null);
            }
        }

        return $this;
    }

    public function getPromotionalCode(): ?PromotionalCode
    {
        return $this->promotionalCode;
    }

    public function setPromotionalCode(?PromotionalCode $promotionalCode): self
    {
        $this->promotionalCode = $promotionalCode;

        return $this;
    }

    public function getPaymentDate(): ?\DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(?\DateTimeInterface $paymentDate): self
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }
}
