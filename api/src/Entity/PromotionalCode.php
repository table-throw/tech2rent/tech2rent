<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\PromotionalCode\CheckPromotionalCode;
use App\Controller\PromotionalCode\GetPromotionalCode;
use App\Controller\PromotionalCode\AddCart;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

// *              
/**
 * @ApiResource(
 *      itemOperations={
 *          "get"={
 *              "security"="is_granted('ROLE_USER')",
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_COMPANY')"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_COMPANY')"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_USER')"
 *          },
 *          "add-cart"={
 *              "method"="PATCH",
 *              "security"="is_granted('ROLE_USER')",
 *              "input_formats"={"json"={"application/merge-patch+json"}},
 *              "path"="/promotional_codes/{uuid_promotional_code}/add-cart/{uuid_cart}",
 *              "controller"=AddCart::class,
 *              "defaults"={"_api_receive"=false},
 *              "openapi_context"={
 *                  "summary"="Add cart into promotional code.",
 *                  "parameters"= {
 *                      {
 *                          "name" : "uuid_cart",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      },
 *                      {
 *                          "name" : "uuid_promotional_code",
 *                          "in" : "path",
 *                          "type" : "string",
 *                          "required" : true,
 *                          "example" : "00000000-0000-0000-0000-000000000000"
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="The cart has been added to promotional code.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      },
 *                      "404"={
 *                          "description"="Promotional code or cart not found.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              },
 *          },
 *     },
 *     collectionOperations={
 *          "get"={
 *              "controller"=GetPromotionalCode::class,
 *           },
 *          "post",
 *          "check_my_promo_code"={
 *              "method"="GET",
 *              "path"="/promotional_codes/check/{code}",
 *              "controller"=CheckPromotionalCode::class,
 *              "defaults"={"_api_receive"=false},
 *              "security"="is_granted('ROLE_USER')",
 *              "openapi_context"={
 *                  "summary"="Check the promotional code",
 *                  "parameters"= {
 *                      {
 *                          "name" : "code",
 *                          "in" : "path",
 *                          "description" : "",
 *                          "type" : "string"
 *                      }
 *                  }
 *              }
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PromotionalCodeRepository")
 */
class PromotionalCode
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"cart_product_item_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cart_product_item_get"})
     */
    private $code;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     */
    private $sold;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPercent;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class)
     */
    private $company;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="promotionalCode")
     */
    private $carts;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->isPercent = false;
        $this->isActive = true;
        $this->carts = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSold(): ?int
    {
        return $this->sold;
    }

    public function setSold(int $sold): self
    {
        $this->sold = $sold;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getIsPercent(): ?bool
    {
        return $this->isPercent;
    }

    public function setIsPercent(bool $isPercent): self
    {
        $this->isPercent = $isPercent;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts[] = $cart;
            $cart->setPromotionalCode($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->contains($cart)) {
            $this->carts->removeElement($cart);
            // set the owning side to null (unless already changed)
            if ($cart->getPromotionalCode() === $this) {
                $cart->setPromotionalCode(null);
            }
        }

        return $this;
    }
}
