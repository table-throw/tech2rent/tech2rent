<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Controller\Product\Search;
use App\Controller\Product\Get;
use App\Controller\Product\Autocomplete;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"products_items_get"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_COMPANY') && is_granted('EDIT', object)"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"products_collection_get_search"}},
 *              "defaults"={"_api_receive"=false},
 *              "path"="/products",
 *              "controller"=Get::class,
 *              "security"="is_granted('ROLE_COMPANY')",
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_COMPANY')",
 *              "denormalization_context"={"groups"={"products_collection_post"}}
 *          },
 *          "search"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"products_collection_get_search"}},
 *              "defaults"={"_api_receive"=false},
 *              "path"="/products/search",
 *              "controller"=Search::class,
 *              "openapi_context"={
 *                  "summary"="Search for products by name.",
 *                  "parameters"= {
 *                      {
 *                          "name" : "query",
 *                          "in" : "query",
 *                          "description" : "",
 *                          "type" : "string"
 *                      },{
 *                          "name" : "category",
 *                          "in" : "query",
 *                          "description" : "",
 *                          "type" : "string"
 *                      },{
 *                          "name" : "description",
 *                          "in" : "query",
 *                          "description" : "",
 *                          "type" : "string"
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="A list of products by Elasticsearch.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "autocomplete"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"products_collection_get_autocomplete"}},
 *              "defaults"={"_api_receive"=false},
 *              "path"="/products/autocomplete/{query}",
 *              "controller"=Autocomplete::class,
 *              "openapi_context"={
 *                  "summary"="Search for products name autocompletion by name.",
 *                  "parameters"= {
 *                      {
 *                          "name": "query",
 *                          "in": "path",
 *                          "type": "string",
 *                          "required": true
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="A list of product names by Elasticsearch.",
 *                          "content"={
 *                              "application/json"={}
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups({"products_collection_get_search", "products_items_get", "carts_items_get", "products_collection_post","company_item_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_collection_get_search", "products_collection_get_autocomplete","products_items_get","carts_items_get", "products_collection_post","company_item_get","cart_product_item_get"})
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Groups({"products_collection_get_search","products_items_get", "carts_items_get", "products_collection_post","company_item_get","cart_product_item_get"})
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     * @Groups({"products_collection_get_search","products_items_get", "carts_items_get", "products_collection_post","company_item_get","cart_product_item_get"})
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get","company_item_get"})
     */
    private $notation;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"products_collection_post"})
     */
    private $isNew;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="product")
     * @Groups({"products_items_get","company_item_get"})
     */
    private $comments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store", inversedBy="products")
     * @Groups({"products_collection_get_search","products_items_get","carts_items_get", "products_collection_post","cart_product_item_get"})
     */
    private $store;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Compose", mappedBy="product")
     */
    private $composes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @Groups({"products_collection_get_search","products_items_get", "products_collection_post"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numProduct;

    /**
     * @ORM\OneToMany(targetEntity=CartProduct::class, mappedBy="product")
     */
    private $cartProducts;

    /**
     * @var MediaObject|null
     * @Groups({"products_collection_get_search","products_items_get", "carts_items_get", "products_collection_post","company_item_get"})
     * @ORM\ManyToMany(targetEntity="App\Entity\MediaObject", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     */
    public $images;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"products_collection_get_search", "products_collection_get_autocomplete","products_items_get","carts_items_get", "products_collection_post","company_item_get"})
     */
    private $caution;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->notation = 0;
        $this->numProduct = bin2hex(random_bytes(4));
        $this->images = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNotation(): ?float
    {
        return $this->notation;
    }

    public function setNotation(?float $notation): self
    {
        $this->notation = $notation;

        return $this;
    }

    public function getIsNew(): ?bool
    {
        return $this->isNew;
    }

    public function setIsNew(bool $isNew): self
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setProduct($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getProduct() === $this) {
                $comment->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Compose[]
     */
    public function getComposes(): Collection
    {
        return $this->composes;
    }

    public function addCompose(Compose $compose): self
    {
        if (!$this->composes->contains($compose)) {
            $this->composes[] = $compose;
            $compose->setOrder($this);
        }

        return $this;
    }

    public function removeCompose(Compose $compose): self
    {
        if ($this->composes->contains($compose)) {
            $this->composes->removeElement($compose);
            // set the owning side to null (unless already changed)
            if ($compose->getOrder() === $this) {
                $compose->setOrder(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
    
    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function __toString()
    {
        $format = "%s\n";
        return sprintf($format, $this->name);
    }

    public function getNumProduct(): ?string
    {
        return $this->numProduct;
    }

    public function setNumProduct(string $numProduct): self
    {
        $this->numProduct = $numProduct;

        return $this;
    }

    /**
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    public function addCartProduct(CartProduct $cartProduct): self
    {
        if (!$this->cartProducts->contains($cartProduct)) {
            $this->cartProducts[] = $cartProduct;
            $cartProduct->setCart($this);
        }

        return $this;
    }

    public function removeCartProduct(CartProduct $cartProduct): self
    {
        if ($this->cartProducts->contains($cartProduct)) {
            $this->cartProducts->removeElement($cartProduct);
            // set the owning side to null (unless already changed)
            if ($cartProduct->getCart() === $this) {
                $cartProduct->setCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MediaObject[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MediaObject $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->addProduct($this);
        }

        return $this;
    }

    public function RemoveImage(MediaObject $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getProducts()->contains($this)) {
                $image->getProducts()->removeElement($this);
            }
        }

        return $this;
    }

    public function getCaution(): ?float
    {
        return $this->caution;
    }

    public function setCaution(?float $caution): self
    {
        $this->caution = $caution;

        return $this;
    }
}
