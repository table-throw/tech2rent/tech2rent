<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\PromotionalCode;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ApplyCode
 * @package App\Controller\Cart
 */
class ApplyCode
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data): JsonResponse
    {
        $uuid = $data->get('uuid_cart');
        $code = $data->get('code');

        /** 404 */
        $cart = $this->entityManager->getRepository(Cart::class)->findOneBy(['id' => $uuid]);
        $promotionalCode = $this->entityManager->getRepository(PromotionalCode::class)->findOneBy(['code' => $code]);

        if (!isset($cart) || !isset($promotionalCode)){
            $response = new JsonResponse(['404' => 'Code or cart not found']);
            return $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }

        /** 200 */

        $price = $cart->getPrice();
        $sold = $promotionalCode->getSold();

        if ($promotionalCode->getIsPercent() == true) {
            $price = $price-($price*($sold/100));

        } else {
            $price = $price-$sold;
        }

        $response = new JsonResponse(['price' => $price]);
        return $response->setStatusCode(JsonResponse::HTTP_OK); // 200
    }
}
