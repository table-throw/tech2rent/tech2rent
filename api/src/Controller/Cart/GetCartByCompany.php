<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\User;
use App\Repository\CartProductRepository;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class Search
 * @package App\Controller\Product
 */
class GetCartByCompany
{
    private $entityManager;
    private $security;
    private $cartRepo;

    public function __construct(
        EntityManagerInterface $entityManager,
        CartProductRepository $cartRepo,
        Security $security
    )
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->cartRepo = $cartRepo;
    }

    public function __invoke()
    {
        /* @var $user User */
        $user = $this->security->getUser();
        
        $cartProducts = $this->cartRepo->findCartsProductByCompany($user->getCompany()->getId());
        foreach ($cartProducts as $cartProduct) {
            if (!empty($cartProduct->getCart()->getPromotionalCode())) {
                if ($cartProduct->getCart()->getPromotionalCode()->getCompany() !== $user->getCompany()->getId()) {
                    $cartProduct->setCart(null);
                }
            }
        }
        
        if (!$cartProducts)$cartProducts = [];
        return $cartProducts;

    }
}
