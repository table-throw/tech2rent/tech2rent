<?php

namespace App\Controller\Cart;

use App\Entity\CartProduct;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Stripe;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;
use App\Entity\Cart;



class CreateCharge extends AbstractController
{
    private $entityManager;
    private $security;
    private $mailer;

    public function __construct(
        EntityManagerInterface $entityManager, Security $security, \Swift_Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->mailer = $mailer;

    }

    public function __invoke(Request $data)
    {
        
        Stripe\Stripe::setApiKey($_SERVER['STRIPE_PRIVATE_TOKEN']);
        $token = $data->get('token');
        /* @var $user User */
        $user = $this->security->getUser();
        
        try {
            /* @var $cart Cart */
            $cart = $user->getActiveCart();
            $amount = 0;
            $arrayProduct = array();

            foreach ($cart->getCartProducts() as $cartProduct) {
                $calculatedPrice = $this->calculatePrice($cartProduct);
                $amount += $calculatedPrice;
                $arrayProduct[] = array(
                    'dateStart' => $cartProduct->getDateStart(),
                    'dateEnd' => $cartProduct->getDateEnd(),
                    'price' => round($calculatedPrice, 2),
                    'name' => $cartProduct->getProduct()->getName(),
                );
            }
            Stripe\Charge::create([
                'amount' => round($amount,2) * 100,
                'currency' => 'eur',
                'source' =>$token,
            ]);

            $cart->setIsActive(false);
            $newCart = new Cart();
            $newCart->setCartReference(uniqid());
            $newCart->setRenter($user);
            $newCart->setIsActive(true);
            $this->entityManager->persist($newCart);

            $user->addCart($newCart);
            $cart->setPrice($amount);
            
            $cart->setPaymentDate(new \DateTime(date('Y-m-d h:i:s')));
            
            $this->entityManager->persist($cart);
            $this->entityManager->flush();

            $cart->setPrice($amount);

            $message = (new \Swift_Message('Votre commande est validée'))
                ->setFrom('no-reply@tech2rent.fr')
                ->setTo($user->getEmail())
                ->setBody($this->renderView(
                // templates/emails/registration.html.twig
                    'emails/commande.html.twig',
                    ['cart' => $cart, 'products' => $arrayProduct]
                ),
                    'text/html');
            $this->mailer->send($message);

        }catch (Exception $e) {
            return [
                'payment' => false
            ];
        }
        
        return [
            'payment' => true
        ];
    }
    
    public function calculatePrice(CartProduct $cartProduct) {
        
        $diff = $cartProduct->getDateEnd()->diff($cartProduct->getDateStart());
        $days = $diff->days;
        if($days == 0) $days = 1;
        
        $price= $cartProduct->getProduct()->getPrice()*$days;
        $promoCode = $cartProduct->getCart()->getPromotionalCode();
        if (isset($promoCode)) {
            if ($cartProduct->getProduct()->getStore()->getCompany()->getId() === $promoCode->getCompany()->getId()) {
                if ($promoCode->getIsPercent()) {
                    $price = $price * (1 - $promoCode->getSold() / 100 );
                } else {
                    $price = $price - $promoCode->getSold();
                }
            }
        }
        
        return $price;
    }
}
