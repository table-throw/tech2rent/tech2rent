<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RemoveProduct
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data): JsonResponse
    {
        $uuid_cart = $data->get('uuid_cart');
        $uuid_product = $data->get('uuid_product');

        $cartProduct = $this->entityManager->getRepository(CartProduct::class)->findOneBy(['product' => $uuid_product,'cart' => $uuid_cart]);
        
        /** 400 */
        if (empty($cartProduct)) {
            $response = new JsonResponse(['404' => 'Product or cart not found']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
            return $response;
        }

        /** 204 */
        $this->entityManager->remove($cartProduct);
        $this->entityManager->flush();
        
        $response = new JsonResponse(['success' => 'Product removed from cart']);
        $response->setStatusCode(JsonResponse::HTTP_NO_CONTENT); // 204

        return $response;
    }
}
