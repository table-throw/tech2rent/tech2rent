<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddProduct
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data): JsonResponse
    {
        $uuid_cart = $data->get('uuid_cart');
        $uuid_product = $data->get('uuid_product');
        
        $cartProduct = $this->entityManager->getRepository(CartProduct::class)->findOneBy(['product' => $uuid_product,'cart' => $uuid_cart]);

        /* @var $cart Cart*/
        $cart = $this->entityManager->getRepository(Cart::class)->findOneBy(['id' => $uuid_cart]);
        /* @var $product Product*/
        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['id' => $uuid_product]);

        /** 404 */
        if (!isset($cart) || !isset($product)) {
            $response = new JsonResponse(['404' => 'Product or cart not found']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }

        /** 400 */
        if (!empty($cartProduct)) {
            $response = new JsonResponse(['400' => 'This cart already have this product']);
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST); // 400
            return $response;
        }

        /** 200 */
        if ($cart && $product) {
            $cartProduct = new CartProduct();
            $cartProduct->setCart($cart);
            $cartProduct->setProduct($product);
            $this->entityManager->persist($cartProduct);
            $this->entityManager->flush();

            $response = new JsonResponse(['success' => 'Product added to cart']);
            $response->setStatusCode(JsonResponse::HTTP_OK); // 200
        }

        return $response;
    }
}
