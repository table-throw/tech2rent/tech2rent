<?php

namespace App\Controller\Cart;



use App\Entity\Cart;
use App\Entity\CartProduct;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DispoProduct
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data): ?JsonResponse
    {
        $result = [];

        $uuid_cart = $data->get('uuid_cart');
//        $uuid_cart = "9b66f8fc-7c54-4f30-9be1-619e2d7b93c0";

        $cart = $this->entityManager->getRepository(Cart::class)->findOneBy(['id' => $uuid_cart]);

        // 404
        if (!isset($cart)) {
            $response = new JsonResponse(['404' => 'Cart not found']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }

        $cart_products = $cart->getCartProducts();

        $current_date = date('Y-m-d');
        $max_date = date('Y-m-d', strtotime($current_date.'+3 months'));


        foreach ($cart_products as $cart_product) {
            $product = $cart_product->getProduct();
            $cart_product = $this->entityManager->getRepository(CartProduct::class)
                ->findForArrayDate($current_date, $max_date, $product->getId());

            $result[$product->getId()] = [];

            foreach ($cart_product as $item) {
                /* @var $item CartProduct */
                $date = [
                    $item->getDateStart()->format('Y-m-d'),
                    $item->getDateEnd()->format('Y-m-d')
                ];
                array_push($result[$product->getId()], $date);
            }
        }
        $response = new JsonResponse($result);
        $response->setStatusCode(JsonResponse::HTTP_OK); // 200

        return $response;
    }
}
