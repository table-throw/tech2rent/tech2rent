<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class Search
 * @package App\Controller\Product
 */
class Get
{
    private $entityManager;
    private $security;
    private $productRepo;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepo,
        Security $security    
    )
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->productRepo = $productRepo;
    }

    public function __invoke(Request $data)
    {
        /* @var $user User */
        $user = $this->security->getUser();

        $carts = $this->entityManager->getRepository(Cart::class)->findBy(['renter' => $user->getId(),'isActive' => false]);
       
        if (!$carts)$carts = [];
        return $carts;

    }
}
