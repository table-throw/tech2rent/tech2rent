<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class GetOwnedActiveCart
{
    private $entityManager;
    private $security;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security
    )
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke()
    {
        $user = $this->security->getUser();
        $owned_cart = $this->entityManager->getRepository(Cart::class)->findOneBy([
            'renter' => $user,
            'isActive' => true
        ]);

        if (!isset($owned_cart)){
            $owned_cart = new Cart();
            $owned_cart->setRenter($user);
            $this->entityManager->persist($owned_cart);
            $this->entityManager->flush();
        }

        return $owned_cart;
    }
}
