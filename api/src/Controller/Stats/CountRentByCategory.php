<?php
namespace App\Controller\Stats;

use App\Entity\Store;
use App\Entity\User;
use App\Repository\CartProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;

class CountRentByCategory
{
    private $cartProductRepository;
    private $security;
    private $entityManager;

    /**
     * CountRentByProduct constructor.
     * @param CartProductRepository $cartProductRepository
     * @param Security $security
     */
    public function __construct(CartProductRepository $cartProductRepository, Security $security, EntityManagerInterface $entityManager)
    {
        $this->cartProductRepository = $cartProductRepository;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data)
    {
        /* @var $user User */
        $user = $this->security->getUser();
        
        $uuid_store = $data->get('uuid_store');

        $store = $this->entityManager->getRepository(Store::class)->findOneBy(['id' => $uuid_store]);
        
        return $this->cartProductRepository->countRentByCategory($user->getCompany()->getId(),$store);
        
    }
}
