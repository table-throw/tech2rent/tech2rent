<?php

namespace App\Controller\PromotionalCode;

use App\Entity\Cart;
use App\Entity\PromotionalCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddCart
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data): JsonResponse
    {
        $uuid_cart = $data->get('uuid_cart');
        $uuid_promotional_code = $data->get('uuid_promotional_code');
        
        $promotional_code = $this->entityManager->getRepository(PromotionalCode::class)->find($uuid_promotional_code);

        /* @var $cart Cart*/
        $cart = $this->entityManager->getRepository(Cart::class)->findOneBy(['id' => $uuid_cart]);
        
        /** 404 */
        if (!isset($cart) || !isset($promotional_code)) {
            $response = new JsonResponse(['404' => 'Promotional code not found']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }
        
        /** 200 */
        if ($cart && $promotional_code) {
            $promotional_code->addCart($cart);
            $this->entityManager->persist($promotional_code);
            $this->entityManager->flush();

            $response = new JsonResponse(['success' => 'Cart added to promotional code.']);
            $response->setStatusCode(JsonResponse::HTTP_OK); // 200
        }

        return $response;
    }
}
