<?php

namespace App\Controller\PromotionalCode;

use App\Entity\PromotionalCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class GetPromotionalCode
{
    private $entityManager;
    private $security;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security
    )
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }
    
    public function __invoke()
    {
        $connectedUser = $this->security->getUser();
        if(in_array('ROLE_COMPANY',$connectedUser->getRoles())){
            $code_list = $this->entityManager->getRepository(PromotionalCode::class)->findBy([
                'company' => $connectedUser->getCompany()->getId()
            ]);
        }else{
            $code_list = $this->entityManager->getRepository(PromotionalCode::class)->findAll();
        }
        
        if (!isset($code_list)) {
            // $response = new JsonResponse(['404' => "This code is not valid."]);
            // $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
            return [];
        }
        
        return $code_list;
    }
}
