<?php

namespace App\Controller\PromotionalCode;

use App\Entity\PromotionalCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CheckPromotionalCode
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }
    
    public function __invoke(Request $data)
    {
        $code = $data->get('code');
        $my_code = $this->entityManager->getRepository(PromotionalCode::class)->findOneBy([
            'code' => $code,
            'isActive' => true
        ]);
        
        if (!isset($my_code)) {
            // $response = new JsonResponse(['404' => "This code is not valid."]);
            // $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
            return [];
        }
        
        return $my_code;
    }
}
