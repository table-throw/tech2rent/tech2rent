<?php

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserPatchPassword
{

    private $security;
    private $passwordEncoder;
    private $entityManager;

    public function __construct(Security $security, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $data)
    {
        $token = $data->get('token');
        $password = $data->get('password');
        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['token' => $token]);


        /** 200 */
        if (!empty($user)) {

            $encodedPassword = $this->passwordEncoder->encodePassword($user, $password);
            $user->setToken(null);
            $user->setIsActive(true);
            $user->setPassword($encodedPassword);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

          
            $response = new JsonResponse(['success' => 'password changed']);
            $response->setStatusCode(JsonResponse::HTTP_OK); // 200
        } else {
            $response = new JsonResponse(['404' => 'There is no user with this token']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }

        return $response;
    }
}
