<?php

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserForgotPassword
{

    private $security;
    private $passwordEncoder;
    private $entityManager;
    private $mailer;

    public function __construct(Security $security, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager, \Swift_Mailer $mailer)
    {
        $this->security = $security;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function __invoke(Request $data)
    {
        $email = $data->get('email');

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

        /** 200 */
        if (!empty($user)) {
            
            $user->setToken(uniqid());
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $message = (new \Swift_Message('Lien pour changer de mot de passe'))
                ->setFrom('no-reply@tech2rent.fr')
                ->setTo($user->getEmail())
                ->setBody($_SERVER['FRONT_URL'].'/resetPassword/new/'.$user->getToken());
            
            $this->mailer->send($message);
            $response = new JsonResponse(['success' => 'email send']);
            $response->setStatusCode(JsonResponse::HTTP_OK); // 200
        } else {
            $response = new JsonResponse(['404' => 'There is no user with this email']);
            $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND); // 404
        }
        return $response;
    }
}
