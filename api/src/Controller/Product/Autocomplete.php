<?php

namespace App\Controller\Product;

use App\Entity\Product;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Autocomplete
 * @package App\Controller\Product
 */
class Autocomplete
{
    private $requestStack;
    private $finder;

    public function __construct(
        RequestStack $requestStack,
        RepositoryManagerInterface $finder
    )
    {
        $this->requestStack = $requestStack;
        $this->finder = $finder;
    }

    public function __invoke(Request $data)
    {
        $query = $data->get('query');

        $result = $this->finder->getRepository(Product::class)->find($query, 10000, []);

        $final = [];
        foreach ($result as $item) {
            if (!in_array($item->getName(), $final)) {
                $final[] = $item->getName();
            }
        }

        return $final;
    }
}
