<?php

namespace App\Controller\Product;

use App\Entity\Product;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MultiMatch;
use Elastica\Query\Nested;
use Elastica\Query\Match;

/**
 * Class Search
 * @package App\Controller\Product
 */
class Search
{
    private $requestStack;
    private $finder;

    public function __construct(
        RequestStack $requestStack,
        RepositoryManagerInterface $finder
    )
    {
        $this->requestStack = $requestStack;
        $this->finder = $finder;
    }

    public function __invoke(Request $data)
    {
        $query = $data->get('query');
        $description = $data->get('description');
        $category = $data->get('category');
        
        $finder = $this->finder->getRepository(Product::class);
        $bool = (new BoolQuery());

        // search in name
        if (!empty($query)) {
            $match = (new MultiMatch())
                ->setQuery($query)
                ->setFields(["name"]);
            $bool->addMust($match);
        } 
        
        //search in description
        if (!empty($description)) {
            $match = (new MultiMatch())
                ->setQuery($description)
                ->setFields(["description"]);
            $bool->addMust($match);
        }
        
        //searching in Category name
        if (!empty($category)) {
            $schoolsTermQuery = new Match();
            $schoolsTermQuery->setField("category.title", $category);
            $schoolsBoolQuery = new BoolQuery();
            $schoolsBoolQuery->addMust($schoolsTermQuery);

            $nestedQuery = new Nested();
            $nestedQuery->setPath("category");
            $nestedQuery->setQuery($schoolsBoolQuery);

            $bool->addMust($nestedQuery);
        }
        
        $elasticaQuery = (new Query($bool));
        $finder->find($elasticaQuery,100);
        
        return $finder->find($elasticaQuery,100);

    }
}
