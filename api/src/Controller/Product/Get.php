<?php

namespace App\Controller\Product;

use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class Search
 * @package App\Controller\Product
 */
class Get
{
    private $entityManager;
    private $security;
    private $productRepo;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepo,
        Security $security    
    )
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->productRepo = $productRepo;
    }

    public function __invoke(Request $data)
    {
        /* @var $user User */
        $user = $this->security->getUser();
        
        $products = $this->productRepo->findByUserCompany($user->getCompany()->getId());
        
        return $products;

    }
}
