<?php

namespace App\Tests\Behat\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ReferenceManager
{

    private $fixtureLoader;
    
    public function __construct(EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->fixtureLoader = $kernel->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
        
    }

    public function load()
    {
        $fixtureDir = __DIR__.'/../../../fixtures';
        $files = scandir($fixtureDir);
        
        foreach ($files as $file) {
            if (strpos($file, '.yaml') !== false) {
                $yamlFiles[] = $fixtureDir.'/'.$file; 
            }
         }
        return $this->fixtureLoader->load($yamlFiles);
    }
    
}
