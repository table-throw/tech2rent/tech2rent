<?php

namespace App\Tests\Behat\Context\Traits;

use Behat\Gherkin\Node\PyStringNode;

trait AuthTrait
{
    /**
     * The token to use with HTTP authentication
     *
     * @var string
     */
    protected $token;

    /**
     * @Given /^I authenticate with user "([^"]*)" and password "([^"]*)"$/
     */
    public function iAuthenticateWithEmailAndPassword($email, $password)
    {
        $payload = new PyStringNode(array('{"email": "'.$email.'","password": "'.$password.'"}'), 0);
        $this->iHaveThePayload($payload);
        $this->iRequest("POST", "authentication_token");
        $this->token = json_decode($this->getLastResponse()->getContent())->token;
    }
}
