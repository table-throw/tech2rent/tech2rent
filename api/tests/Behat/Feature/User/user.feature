@user
Feature: _User_
  Background:
    Given the following fixtures files are loaded:
      | user      |
      | category   |
      | product   |

  Scenario: Test Authentification
    Given I authenticate with user "admin@admin.fr" and password "admin1"
    Given I request "GET /products/[product_1.id]"
    And the response status code should be 200
    Then print last response
  
  Scenario: Create existing user
    Given I have the payload
    """
    {
        "email": "[user_admin.email]",
        "firstName": "admin",
        "lastName": "admin",
        "phone": "0000000000",
        "password": "admin1"
    }
    """
    When I request "POST /users"
    And the response status code should be 500
      
  Scenario: Create user
    Given I have the payload
    """
    {
        "email": "toto@toto.fr",
        "firstName": "toto",
        "lastName": "toto",
        "phone": "0000000000",
        "password": "toto1",
        "isActive": true
    }
    """
    When I request "POST /users"
    And the response status code should be 201
    And I authenticate with user "toto@toto.fr" and password "toto1"
    And the response status code should be 200

  Scenario: Try to update user 
    Given I authenticate with user "user@user.fr" and password "user1"
    Given I have the payload
    """
    {
        "email": "toto@toto.fr",
        "firstName": "toto",
        "lastName": "toto",
        "phone": "0000000000",
        "password": "toto1"
    }
    """
    When I request "PUT /users/[user_1.id]"
    And the response status code should be 403

    Scenario: Update admin information
        Given I authenticate with user "[user_admin.email]" and password "admin1"
        Given I have the payload
        """
        {
            "email": "toto@toto.fr",
            "firstName": "toto",
            "phone": "0000000000",
            "password": "toto1"
        }
        """
        When I request "PUT /users/[user_admin.id]"
        And the response status code should be 200
        And the "email" property should equal "toto@toto.fr"
        And the "lastName" property should equal "admin"

    Scenario: login with password
        Given I authenticate with user "[user_1.email]" and password "my_password"
        When I request "POST /authentication_token"
        And the response status code should be 200


    Scenario: login wrong password
        Given I have the payload
        """
        {
            "email": "[user_1.email]",
            "password": "toto1"
        }
        """
        When I request "POST /authentication_token"
        And the response status code should be 401
