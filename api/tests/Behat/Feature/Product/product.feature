@product
Feature: _product_
  Background:
    Given the following fixtures files are loaded:
      | user          |
      | product       |
      | category      |

  Scenario: Get populated products as admin
    Given I have loaded elastica
    When I request "GET /products/search"
    And the "hydra:totalItems" property should be an integer equalling "10"
    And the response status code should be 200

  Scenario: Get populated products by query as admin
    Given I have loaded elastica
    When I request "GET /products/search?query=iphone"
    And the response status code should be 200
    And the "hydra:member" property is an array and should contain "iphone"
   
  Scenario: get one product
    Given I request "GET /products/[product_1.id]"
    And the response status code should be 200
 
  Scenario: Create products without aut
    Given I have the payload
    """
    {
        "name": "produit",
        "price": "11.2",
        "description": "testest",
        "notation": "",
        "isNew": true,
        "category": "/categories/[category_1.id]"
    }
    """
    When I request "POST /products"
    And the response status code should be 401

 Scenario: update  with PATCH  products without aut
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PATCH /products/[product_1.id]"
    And the response status code should be 401

 Scenario: update  with PUT  products without aut
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PUT /products/[product_1.id]"
    And the response status code should be 401

  Scenario: Delete products without aut
    Given I request "DELETE /products/[product_1.id]"
    And the response status code should be 401

