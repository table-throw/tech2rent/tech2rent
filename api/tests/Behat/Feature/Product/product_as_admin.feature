@product_admin
Feature: _product_
  Background:
    Given the following fixtures files are loaded:
      | user          |
      | category      |
      | product       |
    Given I authenticate with user "[user_admin.email]" and password "admin1"

  Scenario: Create products as admin
    Given I have the payload
    """
    {
        "name": "produit",
        "price": 11.2,
        "description": "testest",
        "notation": 2.1,
        "isNew": true,
        "category": "/categories/[category_1.id]"
    }
    """
    When I request "POST /products"
    And the response status code should be 201

  Scenario: update with PUT categories  as admin
    Given I have the payload
    """
    {
        "name": "produit",
        "price": 11.2,
        "description": "testest",
        "notation": 2.1,
        "isNew": true,
        "category": "/categories/[category_1.id]"
    }
    """
    When I request "PUT /products/[product_1.id]"
    And the response status code should be 200
    And the "name" property should equal "produit"

  Scenario: update with PATCH categories  as admin
    Given I have the payload
    """
    {
        "name": "produit"
    }
    """
    When I request "PATCH /products/[product_1.id]"
    And the response status code should be 200

  Scenario: Delete categories  as admin
    Given I request "DELETE /products/[product_1.id]"
    And the response status code should be 204

  Scenario: Get populated products as admin
    Given I have loaded elastica
    When I request "GET /products/search"
    And the "hydra:totalItems" property should be an integer equalling "10"  
    And the response status code should be 200
    
  Scenario: Get populated products by query as admin
    Given I have loaded elastica
    When I request "GET /products/search?query=iphone"
    And the response status code should be 200
    Then print last response
    
  Scenario: Get populated products by query and description as admin
    Given I have loaded elastica
    When I request "GET /products/search?query=iphone&description=accusamus"
    And the response status code should be 200
    Then print last response
    
