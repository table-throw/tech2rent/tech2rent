@product_user
Feature: _category_
  Background:
    Given the following fixtures files are loaded:
      | user          |
      | category      |
      | product      |
    Given I authenticate with user "[user.email]" and password "user1"

  Scenario: get one products  as user
    Given I request "GET /products/[product_1.id]"
    And the response status code should be 200

  Scenario: Create products as user
    Given I have the payload
    """
    {
      "title": "Test test",
      "description" : "Test !"
    }
    """
    When I request "POST /products"
    And the response status code should be 403

  Scenario: update with PUT products  as user
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PUT /products/[product_1.id]"
    And the response status code should be 403

  Scenario: update with PATCH products  as user
    Given I have the payload
    """
    {
      "title": "lskdqsdqsldkj"
    }
    """
    When I request "PATCH /products/[product_1.id]"
    And the response status code should be 403

  Scenario: Delete categories without aut
    Given I request "DELETE /products/[product_1.id]"
    And the response status code should be 403

