@cat_user
Feature: _category_
  Background:
    Given the following fixtures files are loaded:
      | user          |
      | category      |
    Given I authenticate with user "[user.email]" and password "user1"

  Scenario: Get all categories as user
    Given I request "GET /categories"
    And the response status code should be 200
   
  Scenario: get one categories  as user
    Given I request "GET /categories/[category_1.id]"
    And the response status code should be 200

  Scenario: Create categories as user
    Given I have the payload
    """
    {
      "title": "Test test",
      "description" : "Test !"
    }
    """
    When I request "POST /categories"
    And the response status code should be 403

 Scenario: update with PUT categories  as user
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PUT /categories/[category_1.id]"
    And the response status code should be 403

 Scenario: update with PATCH categories  as user
    Given I have the payload
    """
    {
      "title": "lskdqsdqsldkj"
    }
    """
    When I request "PATCH /categories/[category_1.id]"
    And the response status code should be 403

  Scenario: Delete categories without aut
    Given I request "DELETE /categories/[category_1.id]"
    And the response status code should be 403

