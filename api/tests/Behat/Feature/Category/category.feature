@cat
Feature: _category_
  Background:
    Given the following fixtures files are loaded:
      | category      |

  Scenario: Get all categories without aut
    Given I request "GET /categories"
    And the response status code should be 200
   
  Scenario: get one category
    Given I request "GET /categories/[category_1.id]"
    And the response status code should be 200
 
  Scenario: Create categories without aut
    Given I have the payload
    """
    {
      "title": "Smartphone Android",
      "description" : "Test !"
    }
    """
    When I request "POST /categories"
    And the response status code should be 401

 Scenario: update  with PATCH  categories without aut
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PATCH /categories/[category_1.id]"
    And the response status code should be 401

 Scenario: update  with PUT  categories without aut
    Given I have the payload
    """
    {
      "title": "string",
      "description": "stridqsdng"
    }
    """
    When I request "PUT /categories/[category_1.id]"
    And the response status code should be 401

  Scenario: Delete categories without aut
    Given I request "DELETE /categories/[category_1.id]"
    And the response status code should be 401

