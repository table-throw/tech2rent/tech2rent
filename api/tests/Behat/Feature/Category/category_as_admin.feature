@cat_admin
Feature: _category_
  Background:
    Given the following fixtures files are loaded:
      | user          |
      | category      |
    Given I authenticate with user "[user_admin.email]" and password "admin1"
      
   
  Scenario: get one categories  as admin
    And I request "GET /categories/[category_1.id]"
    And the response status code should be 200

  Scenario: Get all categories as admin
    Given I request "GET /categories"
    And the response status code should be 200

  Scenario: Create categories as admin
    Given I have the payload
    """
    {
      "title": "Smartphone Android"
    }
    """
    When I request "POST /categories"
    And the response status code should be 500
    
  Scenario: Create categories as admin
    Given I have the payload
    """
    {
      "title": "Smartphone Android",
      "description" : "Test !"
    }
    """
    When I request "POST /categories"
    And the response status code should be 201
    And the "title" property should equal "Smartphone Android"


 Scenario: update with PUT categories  as admin
    Given I have the payload
    """
    {
      "title": "string"
    }
    """
    When I request "PUT /categories/[category_1.id]"
    And the response status code should be 200
    And the "title" property should equal "string"

 Scenario: update with PATCH categories  as admin
    Given I have the payload
    """
    {
      "title": "lskdqsdqsldkj"
    }
    """
    When I request "PATCH /categories/[category_1.id]"
    And the response status code should be 200

  Scenario: Delete categories  as admin
    Given I request "DELETE /categories/[category_1.id]"
    And the response status code should be 204

