@comment
Feature: _comment_
  Background:
    Given the following fixtures files are loaded:
      | user      |
      | category      |
      | product      |
      | comment      |

  Scenario: Get all comments without aut
    Given I request "GET /comments"
    And the response status code should be 200
   
  Scenario: get one comment
    Given I request "GET /comments/[comment_1.id]"
    And the response status code should be 200
 
  Scenario: Create comments without aut
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "iphone 4",
        "commentator": "Admin",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "POST /comments"
    And the response status code should be 401

 Scenario: update  with PATCH  comments without aut
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "iphone 4",
        "commentator": "Admin",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "PATCH /comments/[comment_1.id]"
    And the response status code should be 401

 Scenario: update  with PUT  comments without aut
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "iphone 4",
        "commentator": "Admin",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "PUT /comments/[comment_1.id]"
    And the response status code should be 401

  Scenario: Delete comments without aut
    Given I request "DELETE /comments/[comment_1.id]"
    And the response status code should be 401

