@commnt_user
Feature: _comment_
  Background:
    Given the following fixtures files are loaded:
      | user      |
      | category      |
      | product      |
      | comment      |
    Given I authenticate with user "[user.email]" and password "user1"

  Scenario: Get all comments as user
    Given I request "GET /comments"
    And the response status code should be 200
   
  Scenario: get one comments  as user
    Given I request "GET /comments/[comment_1.id]"
    And the response status code should be 200

  Scenario: Create comments as user
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "/products/[product_1.id]",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "POST /comments"
    And the response status code should be 201

 Scenario: update with PUT comments  as user
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "/products/[product_1.id]",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "PUT /comments/[comment_1.id]"
    And the response status code should be 403

 Scenario: update with PATCH comments  as user
    Given I have the payload
    """
    {
        "content": "C'est bien",
        "product": "/products/[product_1.id]",
        "notation": 0,
        "title": "Mon commentaire"
    }
    """
    When I request "PATCH /comments/[comment_1.id]"
    And the response status code should be 403

  Scenario: Delete comments without aut
    Given I request "DELETE /comments/[comment_1.id]"
    And the response status code should be 403

  Scenario: Create and delete comments as user
    Given I request "DELETE /comments/[comment_todelete.id]"
    And the response status code should be 403
