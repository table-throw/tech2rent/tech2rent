## To launch the project
    sudo sysctl -w vm.max_map_count=524288

    docker-compose up
    
``` shell
docker-compose exec php sh -c '
    set -e
    apk add openssl
    mkdir -p config/jwt
    jwt_passphrase=${JWT_PASSPHRASE:-$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')}
    echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
    setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
    setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
'
```

```
docker-compose exec php composer install 
docker-compose exec php php bin/console d:d:d --force
docker-compose exec php php bin/console d:d:c
docker-compose exec php php bin/console d:s:u --force
docker-compose exec php php bin/console d:f:l

docker-compose exec php php bin/console fos:elastica:create
docker-compose exec php php bin/console fos:elastica:populate
```

```
Run tests 
docker-compose exec php vendor/bin/behat
```
curl -X GET "163.172.25.687:9200/_cat/nodes?v&pretty"


```
Production :

docker build --tag tech2rent:1.0 .
docker run -tid -p 8080:80 --name tech2rent tech2rent:1.0
docker run -tid -p 80:80 -p 443:443 --name tech2rent tech2rent:1.0
docker run -tid -p 8080:80 -p 443:443 --name tech2rent tech2rent:1.0
docker run -ti -p 80:80 -p 443:443 --name tech2rent tech2rent:1.0
docker container rm -f tech2rent

docker exec -ti tech2rent sh
docker exec -ti tech2rent_elasticsearch_run_3b8dd0b7d259 sh


ELK :
docker pull docker.elastic.co/elasticsearch/elasticsearch:6.3.2
docker build --tag elk:prod 
docker run -tid -p 9200:9200 --name tech2rent tech2rent:1.0

```


docker pull elasticsearch:6.8.10

docker network create tech2rent
docker run -d --name elasticsearch --net tech2rent -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:6.8.10


php bin/console app:create-user "admin@admin.fr" "admin123" "ROLE_ADMIN"
